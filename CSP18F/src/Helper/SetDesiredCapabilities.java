package Helper;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariOptions;

public class SetDesiredCapabilities {
	GVar gvar= new GVar();
	public SetDesiredCapabilities(GVar gvar)
	{
	this.gvar=gvar;
	}
	

	public static SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MMM_dd HH_mm_ss");
	public static Calendar currentDate = Calendar.getInstance();

	public static String DateWithTime() {
		String datewithtime = formatter.format(currentDate.getTime());
		return datewithtime;
	}

	/*
	 * public DesiredCapabilities DesCap(String Device, int TCNo, String
	 * TCName,String test_type, String environment) {
	 * 
	 * DesiredCapabilities capabilities = null;
	 * 
	 * // if (!Device.contains("-")) Device = Device.concat("- ");
	 * 
	 * // Random r = new Random();
	 * 
	 * switch (Device.split("_")[0]) {
	 * 
	 * 
	 * case "CH": case "Chrome": capabilities = DesiredCapabilities.chrome();
	 * LoggingPreferences logPrefs = new LoggingPreferences();
	 * logPrefs.enable(LogType.PERFORMANCE, Level.INFO);
	 * capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
	 * capabilities.setCapability("version", "latest");
	 * capabilities.setCapability("platform", "Windows 10");
	 * 
	 * Removing platformversion to check whether this causes any issues as per
	 * saucelabs suggestion
	 * 
	 * // capabilities.setCapability("platformVersion", "10");
	 * capabilities.setCapability("screenResolution", "1280x960");
	 * capabilities.setCapability("extendedDebugging", "true");
	 * capabilities.setCapability("name", "Desktop - Chrome -" + TCNo + "_" +
	 * TCName); // IST // capabilities.setCapability("timeZone", "Kolkata" ); // EST
	 * capabilities.setCapability("timeZone", "New_York");
	 * 
	 * // PST // capabilities.setCapability("timeZone", "Los_Angeles" );
	 * 
	 * break; case "FF": capabilities = DesiredCapabilities.firefox();
	 * capabilities.setCapability("version", Device.split("_")[1]);
	 * capabilities.setCapability("platform", "Windows 10");
	 * capabilities.setCapability("platformVersion", "10");
	 * capabilities.setCapability("extendedDebugging", "true");
	 * capabilities.setCapability("screenResolution", "1280x960");
	 * capabilities.setCapability("name", "Desktop - Firefox -" + TCNo + "_" +
	 * TCName); // IST // capabilities.setCapability("timeZone", "Kolkata" ); // EST
	 * capabilities.setCapability("timeZone", "New_York"); // PST //
	 * capabilities.setCapability("timeZone", "Los_Angeles" ); break; case "Edge":
	 * capabilities = DesiredCapabilities.edge();
	 * capabilities.setCapability("version", Device.split("_")[1]);
	 * capabilities.setCapability("platform", "Windows 10");
	 * capabilities.setCapability("platformVersion", "10");
	 * capabilities.setCapability("extendedDebugging", "true");
	 * capabilities.setCapability("screenResolution", "1280x960");
	 * capabilities.setCapability("name", "Desktop - Edge -" + TCNo + "_" + TCName);
	 * // IST // capabilities.setCapability("timeZone", "Kolkata" ); // EST
	 * capabilities.setCapability("timeZone", "New_York"); // PST //
	 * capabilities.setCapability("timeZone", "Los_Angeles" ); break; case "IE":
	 * capabilities = DesiredCapabilities.internetExplorer();
	 * capabilities.setCapability("version", Device.split("_")[1]);
	 * capabilities.setCapability("platform", "Windows 10");
	 * capabilities.setCapability("version", "latest");
	 * capabilities.setCapability("extendedDebugging", "true"); //
	 * capabilities.setCapability("screenResolution", "1920x1200");
	 * capabilities.setCapability("screenResolution", "1280x960");
	 * capabilities.setCapability("name", "Desktop - IE -" + TCNo + "_" + TCName);
	 * // IST // capabilities.setCapability("timeZone", "Kolkata" ); // EST
	 * capabilities.setCapability("timeZone", "New_York"); // PST //
	 * capabilities.setCapability("timeZone", "Los_Angeles" );
	 * capabilities.setCapability("seleniumVersion","3.141.59");
	 * capabilities.setCapability("iedriverVersion","3.141.0");
	 * 
	 * break; case "SF": capabilities = DesiredCapabilities.safari(); //
	 * capabilities.setCapability("version",Device.split("_")[2]);
	 * capabilities.setCapability("platform", "macOS 10.14");
	 * capabilities.setCapability("version", "latest"); //
	 * capabilities.setCapability("screenResolution", "1600x1200");
	 * capabilities.setCapability("screenResolution", "1280x960");
	 * //capabilities.setCapability("nativeWebTap", true);
	 * capabilities.setCapability("extendedDebugging", "true"); //
	 * capabilities.setCapability("screenResolution", "1400x1050");
	 * capabilities.setCapability("name", "Desktop - SF -" + TCNo + "_" + TCName);
	 * // IST // capabilities.setCapability("timeZone", "Kolkata" ); // EST
	 * capabilities.setCapability("timeZone", "New_York"); // PST //
	 * capabilities.setCapability("timeZone", "Los_Angeles" ); break;
	 * 
	 * 
	 * case "Desktop": ChromeOptions options1 = new ChromeOptions();
	 * options1.addArguments("--disable-notifications");
	 * capabilities.setCapability("goog:chromeOptions", options1);
	 * capabilities.setCapability("version", "78");
	 * capabilities.setCapability("platform", "Windows");
	 * capabilities.setCapability("platformVersion", "10");
	 * capabilities.setCapability("screenResolution", "1280x960");
	 * capabilities.setCapability("name", "Desktop - Chrome -" + TCNo + "_" +
	 * TCName); break;
	 * 
	 * 
	 * case "DesktopHeadless": // updated by Nishant @ 29 Jul 2019 ChromeOptions
	 * chromeOpt = new ChromeOptions(); chromeOpt.addArguments("--headless");
	 * capabilities.setCapability("goog:chromeOptions", chromeOpt);
	 * capabilities.setCapability("browserName", "chrome");
	 * capabilities.setCapability("browserVersion", "latest");
	 * capabilities.setCapability("platformName", "Windows 10");
	 * capabilities.setCapability("screenResolution", "1280x960");
	 * capabilities.setCapability("name", "DesktopHeadless -" + TCName); break;
	 * 
	 * }
	 * 
	 * capabilities.setCapability("build",
	 * "PWA 4.2_"+test_type+"_"+environment+"_"+DateWithTime());
	 * capabilities.setCapability("tags", "PWA_4.2_"+test_type+"_"+environment);
	 * 
	 * return capabilities; }
	 * 
	 */
public RemoteWebDriver MutCap(String Device, int TCNo, String TCName,String test_type, String environment, RemoteWebDriver driver, String URL) throws MalformedURLException {
		
		MutableCapabilities sauceOptions = new MutableCapabilities();
		sauceOptions.setCapability("seleniumVersion", "3.141.59");
		sauceOptions.setCapability("idleTimeout", "900");
		//sauceOptions.setCapability("extendedDebugging", "true");
		sauceOptions.setCapability("timeZone", "New_York");
		sauceOptions.setCapability("build", "PWA 4.2_"+test_type+"_"+environment+"_"+DateWithTime());
		sauceOptions.setCapability("tags", "PWA_4.2_"+test_type+"_"+environment);	
		if (environment.toLowerCase().contains("prod")||environment.toLowerCase().contains("uat2")) {
			sauceOptions.setCapability("extendedDebugging", true);
		}
		if (environment.toLowerCase().startsWith("performance")) {
			sauceOptions.setCapability("capturePerformance", true);	
			sauceOptions.setCapability("extendedDebugging", true);
		}
		
		if (test_type.toLowerCase().startsWith("largeorder"))
			sauceOptions.setCapability("maxDuration", "10800");
//		if(test_type.toLowerCase().contains("chat")||gvar.getTCName().toLowerCase().contains("genesyslivechat"))
//			sauceOptions.setCapability("idleTimeout", "300");
		gvar.setBuild(sauceOptions.getCapability("build").toString());	
	
		
		if(Device.split("_").length==1)
			Device=Device+"_latest";		
		Device=Device.toLowerCase();
	/*	if(gvar.getBrand().contains("18F") && !environment.toLowerCase().contains("cbt") && !gvar.getTCName().contains("CardIsle") && !gvar.getTCName().contains("PLA")) {
			Device="ff_latest";
		}
		else */
			Device=Device.toLowerCase();
		switch (Device.split("_")[0]) {
		
		case "ch":	
		case "chrome":
			
			sauceOptions.setCapability("screenResolution", "1680x1050");
			sauceOptions.setCapability("name", "Desktop - Chrome -" + TCNo + "_" + TCName);
			ChromeOptions browserOptions = new ChromeOptions();
			browserOptions.addArguments("--disable-notifications");
			browserOptions.setExperimentalOption("w3c", true);
			browserOptions.setCapability("platformName", "Windows 10");
			browserOptions.setCapability("browserVersion", Device.split("_")[1]);
//				 browserOptions.setCapability("browserVersion", "103");
			if(TCNo==219)
			browserOptions.addArguments("user-agent=\"Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)\"");	
			browserOptions.setCapability("sauce:options", sauceOptions);
			
			gvar.setBrowserVersion(browserOptions.getCapability("browserVersion").toString());		
			driver = new RemoteWebDriver(new URL(URL), browserOptions);
			break;
		case "ch90":
			
			sauceOptions.setCapability("screenResolution", "1680x1050");
			sauceOptions.setCapability("name", "Desktop - Chrome -" + TCNo + "_" + TCName);
			ChromeOptions browserOptionsch90 = new ChromeOptions();
			browserOptionsch90.setExperimentalOption("w3c", true);
			browserOptionsch90.setCapability("platformName", "Windows 10");
			browserOptionsch90.setCapability("browserVersion", Device.split("_")[1]);
			browserOptionsch90.setCapability("sauce:options", sauceOptions);
			gvar.setBrowserVersion(browserOptionsch90.getCapability("browserVersion").toString());		
			driver = new RemoteWebDriver(new URL(URL), browserOptionsch90);
			break;
		case "ff":
		case "firefox":
			sauceOptions.setCapability("screenResolution", "1280x960");
			sauceOptions.setCapability("name", "Desktop - Firefox -" + TCNo + "_" + TCName);
			FirefoxOptions ffOptions = new FirefoxOptions();
			ffOptions.setCapability("platformName", "Windows 10");
			ffOptions.setCapability("browserVersion", Device.split("_")[1]);
			ffOptions.setCapability("sauce:options", sauceOptions);	
			driver = new RemoteWebDriver(new URL(URL), ffOptions);
			break;
		case "edge":		
			sauceOptions.setCapability("screenResolution", "1280x960");
			sauceOptions.setCapability("name", "Desktop - Edge -" + TCNo + "_" + TCName);
			EdgeOptions edgeOptions = new EdgeOptions();
			edgeOptions.setCapability("platformName", "Windows 10");
			edgeOptions.setCapability("browserVersion", Device.split("_")[1]);
			edgeOptions.setCapability("sauce:options", sauceOptions);
			driver = new RemoteWebDriver(new URL(URL), edgeOptions);
			
			break;
		case "ie":
		case "internet explorer":
			//sauceOptions.setCapability("screenResolution", "2560x1600");
			sauceOptions.setCapability("screenResolution", "1280x960");
			sauceOptions.setCapability("name", "Desktop - IE -" + TCNo + "_" + TCName);
			InternetExplorerOptions ieOptions = new InternetExplorerOptions();
			ieOptions.setCapability("platformName", "Windows 10");
			ieOptions.setCapability("browserVersion", Device.split("_")[1]);
			ieOptions.setCapability("sauce:options", sauceOptions);
			ieOptions.setCapability("iedriverVersion", "3.141.59");
			ieOptions.setCapability("build", "PWA 4.2_"+test_type+"_"+environment+"_"+DateWithTime());
			gvar.setPlatform("IE_"+Device.split("_")[1]);
			driver = new RemoteWebDriver(new URL(URL), ieOptions);
			break;
		case "sf":
		case "safari":
			sauceOptions.setCapability("screenResolution", "1600x1200");
			sauceOptions.setCapability("name", "Desktop - SF -" + TCNo + "_" + TCName);
			SafariOptions sfOptions = new SafariOptions();
			//sfOptions.setCapability("platformName", "macOS 10.15");
			//sfOptions.setCapability("browserVersion", Device.split("_")[1]);
			sfOptions.setCapability("browserVersion", Device.split("_")[1]);
			//sfOptions.setCapability("safaridriverVersion", "3.141.59");
//			sfOptions.setCapability("safaridriverVersion", "3.13.0");
			sfOptions.setCapability("sauce:options", sauceOptions);
			gvar.setPlatform("SF_"+Device.split("_")[1]);
			driver = new RemoteWebDriver(new URL(URL), sfOptions);
			break;
		}
		/*if(gvar.getTCName().toLowerCase().contains("pla")) {
		sauceOptions.setCapability("User-Agent", "Googlebot");	
		}*/
		gvar.setJobName(sauceOptions.getCapability("name").toString());
		System.out.println(gvar.getTCName() + sauceOptions.getCapability("seleniumVersion"));	
		driver.manage().window().maximize();
		return driver;
	}
}
