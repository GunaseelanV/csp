package Helper;

import java.awt.Graphics2D;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WindowType;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.csvreader.CsvWriter;

//import Pages.HomePage;
//import io.appium.java_client.remote.AndroidMobileCapabilityType;
import jxl.Cell;
import jxl.CellView;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.saucelabs.saucerest.SauceREST;


public class HelperMethods {

	GVar gvar = new GVar();
	RemoteWebDriver driver;
	JavascriptExecutor js;

	private static ReentrantLock lock = new ReentrantLock();
	private static ReentrantReadWriteLock.ReadLock readlock;
	private static ReentrantReadWriteLock.WriteLock writelock;

	public HelperMethods(GVar gvar) {
		this.gvar = gvar;
		js = (JavascriptExecutor) driver; 

	}

	public void deleteCookies(WebDriver driver) {
		if(!gvar.getPlatform().toUpperCase().contains("IE"))
		{
			System.out.println("DELETING COOKIES");
			driver.manage().deleteAllCookies();
		}
	}

	public static SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MMM_dd HH_mm_ss");
	public static Calendar currentDate = Calendar.getInstance();

	public static String DateWithTime() {
		String datewithtime = formatter.format(currentDate.getTime());
		return datewithtime;
	}

	public static String getTime() {// created by Nishant @ 04 Mar 2019
		return new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
	}

	public static String setFramework(String FilePath) throws Exception {
		// get current date in proper format
		String datenow = DateWithTime().split(" ")[0].replace("/", "_");
		new File(FilePath + datenow).mkdirs();
		new File(FilePath + datenow + "/Passed Screenshots").mkdirs();
		new File(FilePath + datenow + "/Failed Screenshots").mkdirs();
		new File(FilePath + datenow + "/Error Screenshots").mkdirs();
		new File(FilePath + datenow + "/Skipped Screenshots").mkdirs();
		return FilePath + datenow;
	}

	public ArrayList<String> readXlsRow(int RowNumber) throws IOException, InterruptedException {// Created by Nishant @
		// 22 Oct 2018
		ArrayList<String> TestData = new ArrayList<String>();
		File inputWorkbook = new File(gvar.getDataFilePath() + ".xls");
		boolean lockAcquired = lock.tryLock(120, TimeUnit.SECONDS);
		tryagain: if (lockAcquired) {
			try {
				try {
					if (inputWorkbook.exists()) {
						Workbook w = Workbook.getWorkbook(inputWorkbook);
						Sheet sheet = w.getSheet(0);
						for (int i = 0; i < sheet.getColumns(); i++) {
							Cell cellValue = sheet.getCell(i, RowNumber);
							TestData.add(cellValue.getContents());
						}
					} else
						Thread.sleep(1000);
				} catch (BiffException e) {
					e.printStackTrace();
				}
			} finally {
				lock.unlock();
			}
		} else {
			System.out.println("Lock acquired while trying to write");
			System.out.println("On hold" + lock.getHoldCount());
			System.out.println("Queue Length" + lock.getQueueLength());
			break tryagain;
		}
		return TestData;
	}

	public static ArrayList<String> readXlsColumn(String FilePath, int columnNo, int RowFrom, int RowTill)
			throws IOException, InterruptedException { // Created by Nishant @ 31 Oct 2018
		ArrayList<String> al_Data = new ArrayList<String>();
		try {
			for (int i = RowFrom; i <= RowTill; i++) {
				al_Data.add(Workbook.getWorkbook(new File(FilePath + ".xls")).getSheet(0).getCell(columnNo, i)
						.getContents());
			}
		} catch (BiffException e) {
			e.printStackTrace();
		}
		return al_Data;
	}

	public void WriteToExcel(String FileName, int Column, int Row, String Content) throws Exception {// Created by
		// Nishant @ 22
		// Oct 2018
		// ResultcsvHeader("/OCResult.csv");
		ResultcsvHeader(FileName);
		File file = new File(gvar.getDataFilePath() + ".xls");
		WorkbookSettings wbSettings = new WorkbookSettings();
		wbSettings.setLocale(new Locale("en", "EN"));

		boolean lockAcquired = lock.tryLock(120, TimeUnit.SECONDS);
		tryagain: if (lockAcquired) {

			try {
				Workbook workbook = Workbook.getWorkbook(file);
				WritableWorkbook copy = Workbook.createWorkbook(new File(gvar.getDataFilePath() + ".xls"), workbook);
				WritableSheet excelSheet = copy.getSheet(0);
				createLabel(excelSheet, Column, Row, Content);
				copy.write();
				copy.close();
			}

			finally {
				lock.unlock();
			}
		} else {
			System.out.println("Lock acquired while trying to write");
			System.out.println("On hold" + lock.getHoldCount());
			System.out.println("Queue Length" + lock.getQueueLength());
			break tryagain;
		}
	}

	private WritableCellFormat times; // font type ""

	private void createLabel(WritableSheet sheet, int Column, int Row, String Content) throws WriteException {
		// create a times font
		WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
		times = new WritableCellFormat(times10pt); // Define the cell format
		times.setWrap(false); // automatically wrap the cells
		CellView cv = new CellView();
		cv.setFormat(times);
		Label label = new Label(Column, Row, Content, times);
		sheet.addCell(label);
	}

	public void ResultcsvHeader(String FileName) throws Exception {// created by Nishant @ 14 Dec 2018
		String filePath = gvar.getResultPath() + FileName;
		File file = new File(filePath);
		if (!file.exists()) {
			ArrayList<String> Header = new ArrayList<String>();
			Header.add("Objective");
			Header.add("Test Case");
			Header.add("Sr number");
			Header.add("ExpressChk Page");
			Header.add("Run Status");
			Header.add("Note");
			Header.add("TCName");
			Header.add("User type");
			Header.add("User ID");
			Header.add("Password");
			Header.add("Url");
			Header.add("Domain");
			Header.add("Brand");
			Header.add("Line Item Number");
			Header.add("Product Category");
			Header.add(" ProductSku");
			Header.add("Qty");
			Header.add("Product Price");
			Header.add("cutoff");
			Header.add("ManufacturingDays");
			Header.add("Logical Order");
			Header.add("Title");
			Header.add("Fname");
			Header.add("Lname");
			Header.add("Location");
			Header.add("LocationName");
			Header.add("Address1");
			Header.add("Address2");
			Header.add("City");
			Header.add("State");
			Header.add("Zip");
			Header.add("Tax Rate");
			Header.add("Recipient's telephone");
			Header.add("UseAsMyBillingAddress");
			Header.add("DeliveryOption");
			Header.add("Calculated DeliveryDate(FLEX/Exact day)");
			Header.add("Occasion");
			Header.add("MessageOption");
			Header.add("GiftMessage");
			Header.add("Wrapup");
			Header.add("E-mail");
			Header.add("Promotion type");
			Header.add("Promotion Code");
			Header.add("Promotion Value");
			Header.add("Payment Type");
			Header.add("$30 GC");
			Header.add("$1000 GC");
			Header.add("GC amount remaining");
			Header.add("GC Message");
			Header.add("CardType");
			Header.add("CCNumber");
			Header.add("Expiration Month");
			Header.add("Expiration Year");
			Header.add("Name on Card");
			Header.add("Security code");
			Header.add("RnP Order Summery");
			Header.add("OCP Order Summery");
			Header.add("OCP Billing");
			Header.add("OCP Payment");
			Header.add("OCP Item info");
			Header.add("Order Number");
			Header.add("Order Date");
			Header.add("Order Time");
			Header.add("Time taken");
			Header.add("Result");
			Header.add("Comment");
			Header.add("TaxInWebsite");
			Header.add("ExpectedTax");
			Header.add("TaxResult");

			writeCsv(filePath, Header);
		}
	}

	public void WriteResult(String TC, String Msg, String UpdateStatus, String timetaken, String resultFileName)
			throws Exception {
		// Reviewed by Nishant @ 04 Mar 2019
		ResultcsvHeader(resultFileName);
		String csvOutputFile = gvar.getResultPath() + "\\" + resultFileName + ".csv";
		boolean isFileExist = new File(csvOutputFile).exists();
		try {
			// create a FileWriter constructor to open a file in appending mode
			CsvWriter testcases = new CsvWriter(new FileWriter(csvOutputFile, true), ',');
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy_HH:mm:ss");
			Date date = new Date();
			String crntdate = dateFormat.format(date);

			// write header column if the file did not already exist
			if (!isFileExist) {
				testcases.write("TestCaseName");
				testcases.write("Status");
				testcases.write("Comments");
				testcases.write("Time Taken");
				testcases.write("DateTime");
				testcases.write("Email ID");
				testcases.write("Password");
				testcases.write("Machine Name");
				testcases.write("User");
				testcases.write("url tested");
				testcases.endRecord();
			}
			// add record to the file
			testcases.write(TC);
			testcases.write(UpdateStatus);
			testcases.write(gvar.getComment());
			testcases.write(timetaken);
			testcases.write(crntdate);
			testcases.write(gvar.getSignInEmail());
			testcases.write(gvar.getSignInPswd());
			testcases.write(System.getenv("COMPUTERNAME"));
			testcases.write(System.getProperty("user.name"));
			testcases.write(gvar.getUrl());
			testcases.endRecord();
			// testcases.write("\n");
			testcases.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void writeCsv(String csvfile, ArrayList<String> Data) throws Exception {// created by Nishant @14 Dec 2018
		FileWriter fileWriter = new FileWriter(csvfile, true);
		for (int i = 0; i < Data.size(); i++) {
			fileWriter.append(Data.get(i).replace("\n", ""));
			fileWriter.append(",");
		}
		fileWriter.append("\n");
		fileWriter.close();
	}

	public void WriteToExcelFunctionality(String FileName, int Column, int Row, String Content) throws Exception {// Created
		// by
		// Nishant
		// @
		// 22
		// Oct
		// 2018
		// ResultcsvHeader("/OCResult.csv");
		WriteHeaderFunctionality(gvar.getResultPath() + FileName);
		// File file = new File(gvar.getDataFilePath()+".xls");
		WorkbookSettings wbSettings = new WorkbookSettings();
		wbSettings.setLocale(new Locale("en", "EN"));

		boolean lockAcquired = lock.tryLock(120, TimeUnit.SECONDS);
		tryagain: if (lockAcquired) {

			try {
				File file = new File(gvar.getResultPath() + FileName);
				Workbook workbook = Workbook.getWorkbook(file);
				WritableWorkbook wkbk = Workbook.createWorkbook(new File(gvar.getResultPath() + FileName), workbook);
				WritableSheet excelSheet = wkbk.getSheet(0);
				createLabel(excelSheet, Column, Row, Content);
				wkbk.write();
				wkbk.close();

			} finally {
				lock.unlock();
			}
		} else {
			System.out.println("Lock acquired while trying to write");
			System.out.println("On hold" + lock.getHoldCount());
			System.out.println("Queue Length" + lock.getQueueLength());
			break tryagain;
		}
	}

	public void WriteHeaderFunctionality(String FileName) throws Exception {
		WorkbookSettings wbSettings = new WorkbookSettings();
		wbSettings.setLocale(new Locale("en", "EN"));
		boolean lockAcquired = lock.tryLock(120, TimeUnit.SECONDS);
		tryagain: if (lockAcquired) {

			try {
				File filepath = new File(FileName);

				if (!filepath.exists()) {
					WritableWorkbook wkbk = Workbook.createWorkbook(filepath);
					wkbk.createSheet("Result", 0);
					WritableSheet excelSheet = wkbk.getSheet(0);
					createLabel(excelSheet, 0, 0, "Test Case");
					createLabel(excelSheet, 1, 0, "Platform");
					createLabel(excelSheet, 2, 0, "Run Status");
					createLabel(excelSheet, 3, 0, "TCName");
					createLabel(excelSheet, 4, 0, "User ID");
					createLabel(excelSheet, 5, 0, "Password");
					createLabel(excelSheet, 6, 0, "Url");
					createLabel(excelSheet, 7, 0, "Brand");
					createLabel(excelSheet, 8, 0, "Time taken");
					createLabel(excelSheet, 9, 0, "Comment");
					wkbk.write();
					wkbk.close();

				}
			} finally {
				lock.unlock();
			}
		} else {
			System.out.println("Lock acquired while trying to write");
			System.out.println("On hold" + lock.getHoldCount());
			System.out.println("Queue Length" + lock.getQueueLength());
			break tryagain;
		}
	}

	public static String DelDateCal(String SLA) {
		// created by Nishant @ 23 Oct 2018
		SimpleDateFormat sdf = new SimpleDateFormat("d/MM/YYYY");
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(new Date());// today's date instance
		int WeekDay = calDate.get(Calendar.DAY_OF_WEEK);
		// System.out.println("Day of Week: "+WeekDay);

		String deliveryDate = "";

		switch (SLA) {
		case "HOL":
			break;
		case "Flex":
			break;

		case "SLASTD":
		case "SLA5":
			switch (WeekDay) {
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				calDate.add(Calendar.DATE, 7);
				break;
			case 7:
			case 1:
				calDate.add(Calendar.DATE, 8);
				break;
			}
			break;

		case "SLA0":
		case "SLATODAY":
			break;
		case "SLATOMORROW":
			calDate.add(Calendar.DATE, 1);
			break;
		case "SLA1":
			switch (WeekDay) {
			case 2:
			case 3:
			case 4:
			case 5:
				calDate.add(Calendar.DATE, 1);
				break;
			case 6:
			case 7:
				calDate.add(Calendar.DATE, 3);
				break;
			case 1:
				calDate.add(Calendar.DATE, 2);
				break;
			}
			break;

		case "SLA2":
			switch (WeekDay) {
			case 2:
			case 3:
			case 4:
				calDate.add(Calendar.DATE, 2);
				break;
			case 5:
			case 6:
			case 7:
				calDate.add(Calendar.DATE, 4);
				break;
			case 1:
				calDate.add(Calendar.DATE, 3);
				break;
			}
			break;

		case "SLA3":
			switch (WeekDay) {
			case 2:
			case 3:
				calDate.add(Calendar.DATE, 3);
				break;
			case 4:
			case 5:
			case 6:
			case 7:
				calDate.add(Calendar.DATE, 5);
				break;
			case 1:
				calDate.add(Calendar.DATE, 4);
				break;
			}
			break;

		case "SLASAT":
			switch (WeekDay) {
			case 2:
				calDate.add(Calendar.DATE, 5);
				break;
			case 3:
				calDate.add(Calendar.DATE, 4);
				break;
			case 4:
				calDate.add(Calendar.DATE, 3);
				break;
			case 5:
				calDate.add(Calendar.DATE, 2);
				break;
			case 6:
				calDate.add(Calendar.DATE, 1);
				break;
			case 7:
				calDate.add(Calendar.DATE, 8);
				break;
			case 1:
				calDate.add(Calendar.DATE, 7);
				break;
			}
			break;

		case "SLASATNXT":
			switch (WeekDay) {
			case 2:
				calDate.add(Calendar.DATE, 12);
				break;
			case 3:
				calDate.add(Calendar.DATE, 11);
				break;
			case 4:
				calDate.add(Calendar.DATE, 10);
				break;
			case 5:
				calDate.add(Calendar.DATE, 9);
				break;
			case 6:
				calDate.add(Calendar.DATE, 8);
				break;
			case 7:
				calDate.add(Calendar.DATE, 15);
				break;
			case 1:
				calDate.add(Calendar.DATE, 14);
				break;
			}
			break;

		case "SLASUN":
			switch (WeekDay) {
			case 2:
				calDate.add(Calendar.DATE, 6);
				break;
			case 3:
				calDate.add(Calendar.DATE, 5);
				break;
			case 4:
				calDate.add(Calendar.DATE, 4);
				break;
			case 5:
				calDate.add(Calendar.DATE, 3);
				break;
			case 6:
				calDate.add(Calendar.DATE, 2);
				break;
			case 7:
				calDate.add(Calendar.DATE, 1);
				break;
			case 1:
				calDate.add(Calendar.DATE, 8);
				break;
			}
			break;

		case "SLAMON":
			switch (WeekDay) {
			case 2:
				calDate.add(Calendar.DATE, 7);
				break;
			case 3:
				calDate.add(Calendar.DATE, 6);
				break;
			case 4:
				calDate.add(Calendar.DATE, 5);
				break;
			case 5:
				calDate.add(Calendar.DATE, 4);
				break;
			case 6:
				calDate.add(Calendar.DATE, 3);
				break;
			case 7:
				calDate.add(Calendar.DATE, 2);
				break;
			case 1:
				calDate.add(Calendar.DATE, 1);
				break;
			}
			break;

		case "SLA11":
			switch (WeekDay) {
			case 2:
			case 3:
			case 4:
			case 5:
				calDate.add(Calendar.DATE, 15);
				break;
			case 6:
			case 7:
				calDate.add(Calendar.DATE, 17);
				break;
			case 1:
				calDate.add(Calendar.DATE, 16);
				break;
			}
			break;

		case "SLA21":
			switch (WeekDay) {
			case 2:
			case 3:
			case 4:
			case 5:
				calDate.add(Calendar.DATE, 29);
				break;
			case 6:
			case 7:
				calDate.add(Calendar.DATE, 31);
				break;
			case 1:
				calDate.add(Calendar.DATE, 30);
				break;
			}
			break;
		}
		deliveryDate = sdf.format(calDate.getTime());
		System.out.println("Delivery date to be selected " + SLA + "-" + deliveryDate);
		return deliveryDate;
	}

	// Originally created By Nishant @ 3 jan 2018
	// reused at 17 Oct 2018
	private static final String JS_RETRIEVE_DEVICE_PIXEL_RATIO = "var pr = window.devicePixelRatio; if (pr != undefined && pr != null)return pr; else return 1.0;";

	private static void hideScroll(WebDriver driver) {

		((JavascriptExecutor) driver).executeScript("document.documentElement.style.overflow = 'hidden';");

	}

	private static void showScroll(WebDriver driver) {

		((JavascriptExecutor) driver).executeScript("document.documentElement.style.overflow = 'visible';");

	}

	private static void showHideElements(WebDriver driver, Boolean hide, WebElement... skipElements) {

		String display;

		if (hide) {

			display = "none";

		} else {

			display = "block";

		}

		if (skipElements != null) {

			for (WebElement skipElement : skipElements) {

				((JavascriptExecutor) driver).executeScript("arguments[0].style.display = '" + display + "';",
						skipElement);

			}

		}

	}

	private static byte[] getScreenShot(WebDriver driver) {

		return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);

	}

	// The code that does the job

	public static void makeFullScreenshot(WebDriver driver, String strFilename)
			throws IOException, InterruptedException {

		// Scroll to bottom to make sure all elements loaded correctly

		// scrollVerticallyTo(driver, (int) longScrollHeight);

		// scroll up first to start taking screenshots

		scrollVerticallyTo(driver, 0);

		hideScroll(driver);

		// No need to hide elements for first attempt

		byte[] bytes = getScreenShot(driver);

		// showHideElements(driver, true, skipElements);

		long longScrollHeight = (Long) ((JavascriptExecutor) driver).executeScript("return Math.max("

				+ "document.body.scrollHeight, document.documentElement.scrollHeight,"

				+ "document.body.offsetHeight, document.documentElement.offsetHeight,"

				+ "document.body.clientHeight, document.documentElement.clientHeight);"

				);

		BufferedImage image = ImageIO.read(new ByteArrayInputStream(bytes));

		int capturedWidth = image.getWidth();

		int capturedHeight = image.getHeight();

		Double devicePixelRatio = ((Number) ((JavascriptExecutor) driver).executeScript(JS_RETRIEVE_DEVICE_PIXEL_RATIO))
				.doubleValue();

		int scrollHeight = (int) longScrollHeight;

		// File file = File.createTempFile("screenshot", ".png");

		int adaptedCapturedHeight = (int) ((capturedHeight) / devicePixelRatio);

		BufferedImage resultingImage;

		if (Math.abs(adaptedCapturedHeight - scrollHeight) > 40) {

			int scrollOffset = adaptedCapturedHeight;

			int times = scrollHeight / adaptedCapturedHeight;

			int leftover = scrollHeight % adaptedCapturedHeight;

			final BufferedImage tiledImage = new BufferedImage(capturedWidth, (int) ((scrollHeight) * devicePixelRatio),
					BufferedImage.TYPE_INT_RGB);

			Graphics2D g2dTile = tiledImage.createGraphics();

			g2dTile.drawImage(image, 0, 0, null);

			int scroll = 0;

			for (int i = 0; i < times - 1; i++) {

				scroll += scrollOffset;

				scrollVerticallyTo(driver, scroll);

				BufferedImage nextImage = ImageIO.read(new ByteArrayInputStream(getScreenShot(driver)));

				g2dTile.drawImage(nextImage, 0, (i + 1) * capturedHeight, null);

			}

			if (leftover > 0) {

				scroll += scrollOffset;

				scrollVerticallyTo(driver, scroll);

				BufferedImage nextImage = ImageIO.read(new ByteArrayInputStream(getScreenShot(driver)));

				BufferedImage lastPart = nextImage.getSubimage(0,
						nextImage.getHeight() - (int) ((leftover) * devicePixelRatio), nextImage.getWidth(), leftover);

				g2dTile.drawImage(lastPart, 0, times * capturedHeight, null);

			}

			scrollVerticallyTo(driver, 0);

			resultingImage = tiledImage;

		} else {

			resultingImage = image;

		}

		showScroll(driver);

		// showHideElements(driver, false, skipElements);

		ImageIO.write(resultingImage, "png", new File(strFilename));

		// return file;

	}

	private static void scrollVerticallyTo(WebDriver driver, int scroll) {

		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, " + scroll + ");");

		try {

			waitUntilItIsScrolledToPosition(driver, scroll);

		} catch (InterruptedException e) {

			// LOG.trace("Interrupt error during scrolling occurred.", e);

		}

	}

	private static void waitUntilItIsScrolledToPosition(WebDriver driver, int scrollPosition)
			throws InterruptedException {

		int hardTime = 0;// SCREENSHOT_FULLPAGE_SCROLLWAIT

		if (hardTime > 0) {

			Thread.sleep(hardTime);

		}

		int time = 250;// SCREENSHOT_FULLPAGE_SCROLLTIMEOUT

		boolean isScrolledToPosition = false;

		while (time >= 0 && !isScrolledToPosition) {

			Thread.sleep(50);

			time -= 50;

			isScrolledToPosition = Math.abs(obtainVerticalScrollPosition(driver) - scrollPosition) < 3;

		}

	}

	private static int obtainVerticalScrollPosition(WebDriver driver) {

		Long scrollLong = (Long) ((JavascriptExecutor) driver).executeScript(
				"return (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;");

		return scrollLong.intValue();

	}

	public String Getdomain(String Brand) {
		String brand = "";
		switch (Brand) {
		case "18B":
			brand = "1800baskets";
			break;
		case "FBQ":
			brand = "fruitbouquets";
			break;
		case "SCH":
			brand = "simplychocolate";
			break;
		case "BRY":
			brand = "berries";
			break;
		}

		return brand;
	}

	public String getURL(String Env, String Brand) {
		String prefix = "";
		String suffix = "";
		if (Env.equals("prod") || Env.equals("canary"))
			prefix = "pwa.";
		else
			prefix = "uat1-pwa.";

		if (Env.equals("prod"))
			suffix = ".com";
		else if (Env.equals("canary"))
			suffix = ".com/canary";
		else
			suffix = ".dev";
		((JavascriptExecutor)driver).executeScript("window.open()");
		String URL = "https://" + prefix + Getdomain(Brand) + suffix;
		return URL;
	}

	public static void SlackMessage(String testName, int Passcount, int FailedCount, int SkipCount, String ErrMsg,String buildId, String tax, String Note) {
		// String webhookUrl =
		// "https://hooks.slack.com/services/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";
		// //for QA-test_webhook channel
		// String webhookUrl =
		// "https://hooks.slack.com/services/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";
		// for qa_automation-updates
		//		String webhookUrl ="https://hooks.slack.com/services/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";
		// // for qa_pwa_test_results
		// String webhookUrl =
		// "https://hooks.slack.com/services/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";
		
		String webhookUrl = "";
		if (testName.toLowerCase().contains("gdpr"))  // for #cicd-compliance-test-results
			//webhookUrl = "https://hooks.slack.com/services/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";
			webhookUrl = "https://hooks.slack.com/services/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";
		else if (testName.toLowerCase().contains("sci"))   // for #cicd-pwa-sci-results
			webhookUrl = "https://hooks.slack.com/services/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";
		else if(testName.toLowerCase().contains("devopschannel")) // for devops-saucelabs
			webhookUrl ="https://hooks.slack.com/services/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";
		else if(testName.toLowerCase().contains("ssp")) // for #cicd-ssp-test-results
			webhookUrl ="https://hooks.slack.com/services/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";
		else if (testName.toLowerCase().contains("csp"))   // for #cicd-csp-desktop-results
			webhookUrl = "https://hooks.slack.com/services/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";
		else // for #cicd-pwa-desktop-results
			//webhookUrl = "https://hooks.slack.com/services/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";
		webhookUrl = "https://hooks.slack.com/services/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";
			// Define the default message for the Slack notification
		int Totalcount = Passcount + FailedCount + SkipCount;

		// Define the color of the slack message
		String messageColor = "good";
		if (FailedCount > 0) {
			messageColor = "danger";
		} else if (SkipCount > 0) {
			messageColor = "warning";
		}

		// build the httpClient object which will send our request to Slack Webhook
		HttpClient httpClient = HttpClientBuilder.create().build();

		try {
			if (ErrMsg.isEmpty())
				ErrMsg = "No Failures";
			// build the HttpPost request object
			HttpPost request = new HttpPost(webhookUrl);

			// build the HTTP request
			StringEntity params;

			if(!tax.isEmpty())
			{
				if(Note.isEmpty()) {
					params = new StringEntity("{\n" + "\"attachments\": [\n" + "{\n" + "\"color\": \""
							+ messageColor + "\",\n" + "\"pretext\": \"" + testName + "\",\n" + "\"fields\": [\n" + "{\n" + "\"title\": \"Build Name\",\n" + "\"value\": \"" + buildId + "\",\n"
							+ "\"short\": false\n" + "}\n,"+"{\n"
							+ "\"title\": \"Total Number of Tests\",\n" + "\"value\": \"" + Totalcount + "\",\n"
							+ "\"short\": true\n" + "}\n," + "{\n" + "\"title\": \"Tests Passed\",\n" + "\"value\": \""
							+ Passcount + "\",\n" + "\"short\": true\n" + "}\n," + "{\n" + "\"title\": \"Tests Failed\",\n"
							+ "\"value\": \"" + FailedCount + "\",\n" + "\"short\": true\n" + "}\n," + "{\n"
							+ "\"title\": \"Tests Skipped\",\n" + "\"value\": \"" + SkipCount + "\",\n" + "\"short\": true\n"
							+ "}\n," + "{\n" + "\"title\": \"Tax\",\n" + "\"value\": \"" + tax + "\",\n"
							+ "\"short\": false\n" + "}\n,"+"{\n" + "\"title\": \"Failures\",\n" + "\"value\": \"" + ErrMsg + "\",\n"
							+ "\"short\": false\n" + "}\n" + "]\n" + "}\n" + "]\n" + "}"

							);	
				}
				else
				{
					params = new StringEntity("{\n" + "\"attachments\": [\n" + "{\n" + "\"color\": \""
							+ messageColor + "\",\n" + "\"pretext\": \"" + testName + "\",\n" + "\"fields\": [\n" + "{\n" + "\"title\": \"Build Name\",\n" + "\"value\": \"" + buildId + "\",\n"
							+ "\"short\": false\n" + "}\n,"+"{\n"
							+ "\"title\": \"Total Number of Tests\",\n" + "\"value\": \"" + Totalcount + "\",\n"
							+ "\"short\": true\n" + "}\n," + "{\n" + "\"title\": \"Tests Passed\",\n" + "\"value\": \""
							+ Passcount + "\",\n" + "\"short\": true\n" + "}\n," + "{\n" + "\"title\": \"Tests Failed\",\n"
							+ "\"value\": \"" + FailedCount + "\",\n" + "\"short\": true\n" + "}\n," + "{\n"
							+ "\"title\": \"Tests Skipped\",\n" + "\"value\": \"" + SkipCount + "\",\n" + "\"short\": true\n"
							+ "}\n," + "{\n" + "\"title\": \"Tax\",\n" + "\"value\": \"" + tax + "\",\n"
							+ "\"short\": false\n" + "}\n,"+"{\n" + "\"title\": \"Note\",\n" + "\"value\": \"" 
							+ Note + "\",\n"
							+ "\"short\": false\n" + "}\n,"+"{\n" + "\"title\": \"Failures\",\n" + "\"value\": \""
							+ ErrMsg + "\",\n"
							+ "\"short\": false\n" + "}\n" + "]\n" + "}\n" + "]\n" + "}"

							);	
				}

			}
			else if(tax.isEmpty()&& Note.isEmpty()){
				params = new StringEntity("{\n" + "\"attachments\": [\n" + "{\n" + "\"color\": \""
						+ messageColor + "\",\n" + "\"pretext\": \"" + testName + "\",\n" + "\"fields\": [\n" + "{\n" + "\"title\": \"Build Name\",\n" + "\"value\": \"" + buildId + "\",\n"
						+ "\"short\": false\n" + "}\n,"+"{\n"
						+ "\"title\": \"Total Number of Tests\",\n" + "\"value\": \"" + Totalcount + "\",\n"
						+ "\"short\": true\n" + "}\n," + "{\n" + "\"title\": \"Tests Passed\",\n" + "\"value\": \""
						+ Passcount + "\",\n" + "\"short\": true\n" + "}\n," + "{\n" + "\"title\": \"Tests Failed\",\n"
						+ "\"value\": \"" + FailedCount + "\",\n" + "\"short\": true\n" + "}\n," + "{\n"
						+ "\"title\": \"Tests Skipped\",\n" + "\"value\": \"" + SkipCount + "\",\n" + "\"short\": true\n"
						+ "}\n," + "{\n" + "\"title\": \"Failures\",\n" + "\"value\": \"" + ErrMsg + "\",\n"
						+ "\"short\": false\n" + "}\n" + "]\n" + "}\n" + "]\n" + "}"

						);		
			}
			else
			{
				params = new StringEntity("{\n" + "\"attachments\": [\n" + "{\n" + "\"color\": \""
						+ messageColor + "\",\n" + "\"pretext\": \"" + testName + "\",\n" + "\"fields\": [\n" + "{\n" + "\"title\": \"Build Name\",\n" + "\"value\": \"" + buildId + "\",\n"
						+ "\"short\": false\n" + "}\n,"+"{\n"
						+ "\"title\": \"Total Number of Tests\",\n" + "\"value\": \"" + Totalcount + "\",\n"
						+ "\"short\": true\n" + "}\n," + "{\n" + "\"title\": \"Tests Passed\",\n" + "\"value\": \""
						+ Passcount + "\",\n" + "\"short\": true\n" + "}\n," + "{\n" + "\"title\": \"Tests Failed\",\n"
						+ "\"value\": \"" + FailedCount + "\",\n" + "\"short\": true\n" + "}\n," + "{\n"
						+ "\"title\": \"Tests Skipped\",\n" + "\"value\": \"" + SkipCount + "\",\n" + "\"short\": true\n"
						+ "}\n," + "{\n" + "\"title\": \"Note\",\n" + "\"value\": \"" + Note + "\",\n"
						+ "\"short\": false\n" + "}\n,"+"{\n" + "\"title\": \"Failures\",\n" + "\"value\": \"" + ErrMsg + "\",\n"
						+ "\"short\": false\n" + "}\n" + "]\n" + "}\n" + "]\n" + "}"

						);	
			}	
			request.addHeader("content-type", "application/x-www-form-urlencoded");
			request.setEntity(params);

			// Executes the HTTP request
			HttpResponse response = httpClient.execute(request);

		} catch (Exception ex) {
			// handle exception here
		}
	}

	public void WriteToExcelFunctionality(String FileName, String[] data) throws Exception {// Created by Nishant @ 22
		// Oct 2018
		// ResultcsvHeader("/OCResult.csv");
		// WriteHeaderFunctionality(gvar.getResultPath()+FileName);
		// File file = new
		// File(gvar.getDataFilePath()+".xls");
		WorkbookSettings wbSettings = new WorkbookSettings();
		wbSettings.setLocale(new Locale("en", "EN"));

		boolean lockAcquired = lock.tryLock(120, TimeUnit.SECONDS);
		tryagain: if (lockAcquired) {

			try {
				WriteHeaderFunctionality(gvar.getResultPath() + FileName);
				File file = new File(gvar.getResultPath() + FileName);
				Workbook workbook = Workbook.getWorkbook(file);
				WritableWorkbook wkbk = Workbook.createWorkbook(new File(gvar.getResultPath() + FileName), workbook);
				WritableSheet excelSheet = wkbk.getSheet(0);
				int row = excelSheet.getRows();
				createLabel(excelSheet, 0, row, data[0]);
				createLabel(excelSheet, 1, row, data[1]);
				createLabel(excelSheet, 2, row, data[2]);
				createLabel(excelSheet, 3, row, data[3]);
				createLabel(excelSheet, 4, row, data[4]);
				createLabel(excelSheet, 5, row, data[5]);
				createLabel(excelSheet, 6, row, data[6]);
				createLabel(excelSheet, 7, row, data[7]);
				createLabel(excelSheet, 8, row, data[8]);
				createLabel(excelSheet, 9, row, data[9]);

				wkbk.write();
				wkbk.close();

			} finally {
				lock.unlock();
			}
		} else {
			System.out.println("Lock acquired while trying to write");
			System.out.println("On hold" + lock.getHoldCount());
			System.out.println("Queue Length" + lock.getQueueLength());
			break tryagain;
		}
	}

	/*
	 * public boolean VerifyCookieExists(WebDriver driver, String name) {
	 * Set<Cookie> cookie= driver.manage().getCookies(); }
	 */
	public String getValueOfCookieNamed(WebDriver driver, String name) {
		String cookie= name + " cookie is not available";		
		if (!(gvar.getPlatform().split("_")[0]).equals("IE"))
			cookie=driver.manage().getCookieNamed(name).getValue();
		else {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			Set<Cookie> cookies;

		//	if (cookies.size() == 0) { // To support FF and IE
			    String cookiesString =(String) js.executeScript("return document.cookie");
			    cookies = parseBrowserCookies(cookiesString);
			   // System.out.println(cookies);
		//	}
			for (Cookie s : cookies) {
			//   System.out.println(s.getName());
			//   System.out.println(s.getValue());
			   if(s.getName().contains(name)) {
				   cookie=s.getValue();
				   break;
			   }
			}
		}
			return cookie;
	}

	public void deleteCookieNamed(WebDriver driver, String name) {
		driver.manage().deleteCookieNamed(name);
	}
	private Set<Cookie> parseBrowserCookies(String cookiesString) {
	    Set<Cookie> cookies = new HashSet<>();

	    if (StringUtils.isBlank(cookiesString)) {
	        return cookies;
	    }

		/*
		 * Arrays.asList(cookiesString.split("; ")).forEach(cookie -> { String[]
		 * splitCookie = cookie.split("=", 2); cookies.add(new Cookie(splitCookie[0],
		 * splitCookie[1], "/")); });
		 */
	    List<String> arrayCookie=Arrays.asList(cookiesString.split("; "));
	    for(int i=0;i<arrayCookie.size();i++) {
	      String[] splitCookie = arrayCookie.get(i).split("=", 2);
		  cookies.add(new Cookie(splitCookie[0], splitCookie[1], "/"));
	    }
	    return cookies;
	}

	public void addCookie(WebDriver driver, String name, String value) {
		String Domainname = ".1800flowers.com";
		switch (gvar.getBrand()) {
		case "18F":
			Domainname = ".1800flowers.com";
			break;
		case "18B":
			Domainname = ".1800baskets.com";
			break;
		case "FBQ":
			Domainname = ".fruitbouquets.com";
			break;
		case "HD":
			Domainname = ".harryanddavid.com";
			break;
		case "CCO":
			Domainname = ".cheryls.com";
			break;
		case "TPF":
			Domainname = ".thepopcornfactory.com";
			break;
		case "SCH":
			Domainname = ".simplychocolate.com";
			break;
		case "WLF":
		case "WF":
			Domainname = ".wolfermans.com";
			break;
		case "STY":
			Domainname = ".stockyards.com";
			break;
		case "BER":
		case "BRY":
			Domainname = ".berries.com";
			break;
		case "VIT":
			Domainname = ".vitalchoice.com";
			break;
		default:
			System.out.println("Domain not added for cookie in code yet. please update code ");
		}

		Cookie cookie = new Cookie.Builder(name, value).domain(Domainname).path("/").build();
		driver.manage().addCookie(cookie);
	}

	public void WriteToExcelTaxResult(String FileName, String[] data) throws Exception {// Created by Nishant @ 22
		// Oct 2018
		// ResultcsvHeader("/OCResult.csv");
		// WriteHeaderFunctionality(gvar.getResultPath()+FileName);
		// File file = new
		// File(gvar.getDataFilePath()+".xls");
		WorkbookSettings wbSettings = new WorkbookSettings();
		wbSettings.setLocale(new Locale("en", "EN"));

		boolean lockAcquired = lock.tryLock(120, TimeUnit.SECONDS);
		tryagain: if (lockAcquired) {

			try {
				WriteHeaderTaxResult(gvar.getResultPath() + FileName);
				File file = new File(gvar.getResultPath() + FileName);
				Workbook workbook = Workbook.getWorkbook(file);
				WritableWorkbook wkbk = Workbook.createWorkbook(new File(gvar.getResultPath() + FileName), workbook);
				WritableSheet excelSheet = wkbk.getSheet(0);
				int row = excelSheet.getRows();
				createLabel(excelSheet, 0, row, data[0]);
				createLabel(excelSheet, 1, row, data[1]);
				createLabel(excelSheet, 2, row, data[2]);
				createLabel(excelSheet, 3, row, data[3]);
				createLabel(excelSheet, 4, row, data[4]);
				createLabel(excelSheet, 5, row, data[5]);
				createLabel(excelSheet, 6, row, data[6]);
				createLabel(excelSheet, 7, row, data[7]);
				createLabel(excelSheet, 8, row, data[8]);
				createLabel(excelSheet, 9, row, data[9]);
				createLabel(excelSheet, 10, row, data[10]);
				createLabel(excelSheet, 11, row, data[11]);
				createLabel(excelSheet, 12, row, data[12]);

				wkbk.write();
				wkbk.close();

			} finally {
				lock.unlock();
			}
		} else {
			System.out.println("Lock acquired while trying to write");
			System.out.println("On hold" + lock.getHoldCount());
			System.out.println("Queue Length" + lock.getQueueLength());
			break tryagain;
		}
	}

	public void WriteHeaderTaxResult(String FileName) throws Exception {
		WorkbookSettings wbSettings = new WorkbookSettings();
		wbSettings.setLocale(new Locale("en", "EN"));
		boolean lockAcquired = lock.tryLock(120, TimeUnit.SECONDS);
		tryagain: if (lockAcquired) {

			try {
				File filepath = new File(FileName);

				if (!filepath.exists()) {
					WritableWorkbook wkbk = Workbook.createWorkbook(filepath);
					wkbk.createSheet("Result", 0);
					WritableSheet excelSheet = wkbk.getSheet(0);
					createLabel(excelSheet, 0, 0, "Test Case");
					createLabel(excelSheet, 1, 0, "Platform");
					createLabel(excelSheet, 2, 0, "Run Status");
					createLabel(excelSheet, 3, 0, "TCName");

					createLabel(excelSheet, 5, 0, "Url");
					createLabel(excelSheet, 6, 0, "Brand");
					createLabel(excelSheet, 7, 0, "OrderSummary");
					createLabel(excelSheet, 8, 0, "ExpectedTaxRate");
					createLabel(excelSheet, 9, 0, "TaxRateInWebsite");
					createLabel(excelSheet, 10, 0, "Result");
					createLabel(excelSheet, 11, 0, "Comments");

					wkbk.write();
					wkbk.close();

				}
			} finally {
				lock.unlock();
			}
		} else {
			System.out.println("Lock acquired while trying to write");
			System.out.println("On hold" + lock.getHoldCount());
			System.out.println("Queue Length" + lock.getQueueLength());
			break tryagain;
		}
	}

	public void EnvironmentURL(String environment, String Brand) {
		String prefix = "https://www.", domain = ".com";
		if(environment.contains("west"))
			prefix =  "https://www-west.";
		if(environment.contains("east"))
			prefix =  "https://www-east.";
		if(environment.contains("central"))
			prefix =  "https://www-central.";
		if (environment.toLowerCase().contains("uat1")) {
			prefix = "https://uat1-pwa.";
			domain = ".dev";
		} else if (environment.toLowerCase().contains("uat2")) {
			prefix = "https://uat2-pwa.";
			domain = ".dev";
		}else if (environment.toLowerCase().contains("loadtest")) {
			prefix = "https://loadtest1-pwa.";
			domain = ".dev";
		}
		
		else if (environment.toLowerCase().contains("csp")) {
			prefix = "https://uat-1.";
			domain = ".tech";
		}
		//if(Brand.equals("VIT") && environment.contains("prod")) {prefix = "https://beta.";}
		
		
		switch (Brand) {
		
		case "18B":
			gvar.setUrl(prefix + "1800baskets" + domain);
			break;
		case "FBQ":
			gvar.setUrl(prefix + "fruitbouquets" + domain);
			break;
		case "BER":
		case "BRY":
			gvar.setUrl(prefix + "berries.com");
			break;
		case "SCH":
			gvar.setUrl(prefix + "simplychocolate" + domain);
			break;
		default:
			System.out.println("Brand not implemented");
		}
	}

	public String launchCSPURL(String Env, String Brand)throws InterruptedException {
		
		((JavascriptExecutor)driver).executeScript("window.open()");
		
		String URLcsp = "https://uat-1.origin-gcp.18f.tech/csp-ui/service";
		
		return URLcsp;	
		
		
	}
	
public String launchCSPURLNewTab(String Env, String Brand)throws InterruptedException {
		
		((JavascriptExecutor)driver).executeScript("window.open()");
		String prefix = "";
		String suffix = "";
		if (Env.equals("prod") || Env.equals("canary"))
			prefix = "pwa.";
		else
			prefix = "uat1-pwa.";

		if (Env.equals("prod"))
			suffix = ".com";
		else if (Env.equals("canary"))
			suffix = ".com/canary";
		else
			suffix = ".dev";
	
		String URL = "https://uat-1.origin-gcp.18f.tech/csp-ui/service";
		return URL;	
		
		
	}
	
	public String[] SKU(String Brand, String environment) {
		String[] sku = { "" };
		switch (Brand) {
		case "18F":
			if (gvar.getProductCategory().split(";")[gvar.getLineItem()].contains("FPT"))
				sku = new String[] {"191167","174313", "148589", "148682", "167891" };
			else
				sku = new String[] {"174313","1822","18650", "18116","18202", "157625" , "18053", "101656"};		
			break;
		case "18B":
			if(environment.contains("uat1"))
				sku = new String[] {"175237","162529","175214","172803", "93053", "96098", "149540", "93054", "175009"};
			else
				sku = new String[] {"96094","149947G","96659","172803", "93053", "96098", "149540", "93054", "175009"};	
			break;
		case "FBQ":
			if (gvar.getProductCategory().split(";")[gvar.getLineItem()].contains("GPT"))
				sku = new String[] {"192585","192583", "192586"};
			else if(environment.contains("uat1"))
				sku = new String[] {"192558L","161999CH"};
			else
				sku = new String[] {"176765","167217","176936","183079","179068","167276"};
			break;
		case "HD":
			sku = new String[] { "12X","316X", "5000X",  "6X", "26633X" };
			break;
		case "WLF":
		case "WF":
			sku = new String[] {"2305W","2305W", "51014W","5600W",  "2470W", "51320W", "30425W" };
			break;
		case "CCO":
			if(environment.contains("uat1"))
				sku = new String[] {"269891","293111","217361","209371","209451", "220361"};
			else
				sku = new String[] {"272011"," 272011","272001","293111","182641","209371","209451", "220361"};
			break;
		case "TPF":
			if(environment.contains("uat1"))
				sku = new String[] {"P08030", "70373", "T1783TY","68082","P220030","C162313","P228030","C162995","C02775"};
			else
				sku = new String[] {"P153030","C7430","84968","C23802", "P74330","P203030","P220030","C162313","P228030","C162995","C02775"};
			break;
		case "STY":
			sku = new String[] {"73105X","72519X","72677X","1100M" ,"8028M","4241M","7172M","1305M","4770M"};
			break;
		case "SCH":
			if(environment.contains("uat1"))
				sku = new String[] {"26586X","163193","163143","163762","163732", "163486","163532","163701" };
			else
				sku = new String[] {"163904","163732", "163486","163532","163701", "179601","163816","163762" };
			break;
		case "VIT":
			if(environment.contains("uat1"))
				sku = new String[] {"60471H", "60373H"};
			else
				sku = new String[] {"60348H" };
			break;
		case "PZU":
			sku = new String[] { "" };
			break;
		case "BER":
		case "BRY":
			if (gvar.getProductCategory().split(";")[gvar.getLineItem()].contains("GPT"))
				sku = new String[] {"192585","192583", "192586"};
			else
				sku = new String[] {"192560","192566", "192554","192603", "192585" ,"192568","192602", "192607"};
			break;
		default:
			System.out.println("We don't have this brand");
			break;
		}
		return sku;
		
	}
	
	public String ErrorMessage(Exception e, RemoteWebDriver driver) throws InterruptedException {
		//System.out.println(e.getClass());
		String ErrMsg = "";
		String[] exceptionType = e.getClass().toString().split("\\s")[1].split("\\.");
		String[] out = e.toString().split("\\n")[0].split(":");
		String pageSource=driver.getPageSource().toString().toLowerCase();
		if (pageSource.contains("upstream")
				|| pageSource.contains("internal server error")
				|| pageSource.contains("no webpage was found for the web address")) {
			for (final StackTraceElement ste : e.getStackTrace()) {
				if (ste.getClassName().split("\\.").length == 2) {
					ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to SITE NOT AVAILABLE in "
							+ ste.getClassName().split("\\.")[1] + "\n";
					gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
					break;
				}

			}
		}
		else	if(pageSource.contains("404 error"))
		{
			for (final StackTraceElement ste : e.getStackTrace()) {
				if (ste.getClassName().split("\\.").length == 2) {
					ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to 404 ERROR in website in "
							+ ste.getClassName().split("\\.")[1] + "\n";
					gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
					break;
				}

			}

		}
		else	if(pageSource.contains("error 503")||pageSource.contains("status code 503"))
		{
			for (final StackTraceElement ste : e.getStackTrace()) {
				if (ste.getClassName().split("\\.").length == 2) {
					ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to 503 ERROR in website in "
							+ ste.getClassName().split("\\.")[1] + "\n";
					gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
					break;
				}

			}

		}
		else	if(pageSource.contains("oops!, something went wrong"))
		{
			for (final StackTraceElement ste : e.getStackTrace()) {
				if (ste.getClassName().split("\\.").length == 2) {
					ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to OOPS ERROR in website in "
							+ ste.getClassName().split("\\.")[1] + "\n";
					gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
					break;
				}

			}

		}
		else	if(pageSource.contains("there was an unexpected problem")||pageSource.contains("something unexpected occurred"))
		{
			for (final StackTraceElement ste : e.getStackTrace()) {
				if (ste.getClassName().split("\\.").length == 2) {
					ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to UNEXPECTED PROBLEM ERROR in website in "
							+ ste.getClassName().split("\\.")[1] + "\n";
					gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
					break;
				}

			}

		}	else if(pageSource.contains("technical issue"))
		{
			for (final StackTraceElement ste : e.getStackTrace()) {
				if (ste.getClassName().split("\\.").length == 2) {
					ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to TECHNICAL ISSUE ERROR in website in "
							+ ste.getClassName().split("\\.")[1] + "\n";
					gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
					break;
				}

			}

		} 	
		else if(pageSource.contains("category page error"))
		{
			for (final StackTraceElement ste : e.getStackTrace()) {
				if (ste.getClassName().split("\\.").length == 2) {
					ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to CATEGORY PAGE ERROR in "
							+ ste.getClassName().split("\\.")[1] + "\n";
					gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
					break;
				}

			}

		}	
		else if(pageSource.contains("there are currently no products available in this collection"))
		{
			for (final StackTraceElement ste : e.getStackTrace()) {
				if (ste.getClassName().split("\\.").length == 2) {
					ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to CURRENTLY NO PRODUCTS AVAILABLE IN COLLECTION ERROR in "
							+ ste.getClassName().split("\\.")[1] + "\n";
					gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
					break;
				}

			}

		}	
		else if(pageSource.contains("change the address or pick a different date")||pageSource.contains("no longer available")||pageSource.contains("the product you have selected is not available"))
		{
			for (final StackTraceElement ste : e.getStackTrace()) {
				if (ste.getClassName().split("\\.").length == 2) {
					ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to PRODUCT NOT DELIVERABLE/NOT AVAILABLE in "
							+ ste.getClassName().split("\\.")[1] + "\n";
					gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
					break;
				}

			}

		}	
		else if(pageSource.contains("temporary website error"))
		{
			for (final StackTraceElement ste : e.getStackTrace()) {
				if (ste.getClassName().split("\\.").length == 2) {
					ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to TEMPORARY WEBSITE ERROR in "
							+ ste.getClassName().split("\\.")[1] + "\n";
					gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
					break;
				}

			}

		}	
		else if(pageSource.contains("error occured generating preview"))
		{
			for (final StackTraceElement ste : e.getStackTrace()) {
				if (ste.getClassName().split("\\.").length == 2) {
					ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to ERROR OCCURED GENERATING PREVIEW in "
							+ ste.getClassName().split("\\.")[1] + "\n";
					gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
					break;
				}

			}

		}	
		else if(gvar.getComment().contains("Product is not available"))
		{
			for (final StackTraceElement ste : e.getStackTrace()) {
				if (ste.getClassName().split("\\.").length == 2) {
					ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to PRODUCT NOT DELIVERABLE/NOT AVAILABLE in "
							+ ste.getClassName().split("\\.")[1] + "\n";
					gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
					break;
				}

			}

		}	
		else  if(driver.getTitle().toLowerCase().contains("undefined"))
		{
			for (final StackTraceElement ste : e.getStackTrace()) {
				if (ste.getClassName().split("\\.").length == 2) {
					ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to UNDEFINED ERROR in website in "
							+ ste.getClassName().split("\\.")[1] + "\n";
					gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
					break;
				}

			}

		}


		else {
			switch (exceptionType[exceptionType.length - 1]) {

			case "IOException":
				for (final StackTraceElement ste : e.getStackTrace()) {
					if (ste.getClassName().split("\\.").length == 2) {
						ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to I/O FILE EXCEPTION in "
								+ ste.getClassName().split("\\.")[1] + "\n";
						gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
						break;
					}

				}

				break;
			case "FileNotFoundException":
				for (final StackTraceElement ste : e.getStackTrace()) {
					if (ste.getClassName().split("\\.").length == 2) {
						ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to INPUT/OUTPUT FILE MISSING EXCEPTION in "
								+ ste.getClassName().split("\\.")[1] + "\n";
						gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
						break;
					}

				}

				break;
			case "NoSuchElementException":

				System.out.println("Exception::" + out[0] + "Element::" + out[out.length - 1]);
				for (final StackTraceElement ste : e.getStackTrace()) {
					if (ste.getClassName().split("\\.").length == 2) {
						ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to MISSING ELEMENT in "
								+ ste.getClassName().split("\\.")[1] + "\n";
						gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
						break;
					}

				}
				break;
			case "InvalidSelectorException":

				for (final StackTraceElement ste : e.getStackTrace()) {
					if (ste.getClassName().split("\\.").length == 2) {
						ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to INVALID XPATH/LOCATOR in "
								+ ste.getClassName().split("\\.")[1] + "\n";
						gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
						break;
					}

				}
				break;
			case "ElementNotVisibleException":

				for (final StackTraceElement ste : e.getStackTrace()) {
					if (ste.getClassName().split("\\.").length == 2) {
						ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() 
						+ ":Failed due to ELEMENT NOT VISIBLE THROUGH SCRIPT in "
						+ ste.getClassName().split("\\.")[1] + "\n";
						gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
						break;
					}

				}
				break;
			case "ElementNotSelectableException":

				for (final StackTraceElement ste : e.getStackTrace()) {
					if (ste.getClassName().split("\\.").length == 2) {
						ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to ELEMENT NOT SELECTABLE in "
								+ ste.getClassName().split("\\.")[1] + "\n";
						gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
						break;
					}

				}
				break;
			case "TimeoutException":

				for (final StackTraceElement ste : e.getStackTrace()) {
					if (ste.getClassName().split("\\.").length == 2) {
						ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to TIMEOUT in "
								+ ste.getClassName().split("\\.")[1] + "\n";
						gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
						break;
					}

				}
				break;
			case "NoSuchSessionException":

				for (final StackTraceElement ste : e.getStackTrace()) {
					if (ste.getClassName().split("\\.").length == 2) {
						ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to SESSION NOT CREATED in "
								+ ste.getClassName().split("\\.")[1] + "\n";
						gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
						break;
					}

				}
				break;
			case "StaleElementReferenceException":

				for (final StackTraceElement ste : e.getStackTrace()) {
					if (ste.getClassName().split("\\.").length == 2) {
						ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to PAGE LOADING in "
								+ ste.getClassName().split("\\.")[1] + "\n";
						gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
						break;
					}

				}
				break;
			case "NoSuchAttributeException":

				for (final StackTraceElement ste : e.getStackTrace()) {
					if (ste.getClassName().split("\\.").length == 2) {
						ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed due to MISSING ATTRIBUTE in "
								+ ste.getClassName().split("\\.")[1] + "\n";
						gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
						break;
					}

				}
				break;
			case "IndexOutOfBoundsException":

				for (final StackTraceElement ste : e.getStackTrace()) {
					if (ste.getClassName().split("\\.").length == 2) {
						ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() 
						+ ":Failed due to MISSING ELEMENT/LOADING ISSUE in " + ste.getClassName().split("\\.")[1]
								+ "\n";
						gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
						break;
					}

				}
				break;
			case "ElementClickInterceptedException":

				for (final StackTraceElement ste : e.getStackTrace()) {
					if (ste.getClassName().split("\\.").length == 2) {
						ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() 
						+ ":Failed due to LOADING ISSUE/SCROLL ISSUE in " + ste.getClassName().split("\\.")[1]
								+ "\n";
						gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
						break;
					}

				}
				break;
			default:

				for (final StackTraceElement ste : e.getStackTrace()) {

					if (ste.getClassName().split("\\.").length == 2) {
						ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() +":Failed in "
								+ ste.getClassName().split("\\.")[1] + ". We have to fix the script.\n";

						gvar.setComment(gvar.getComment() + "Failed in :" + ste.getClassName() + "." + ste.getMethodName());
						break;
					}

				}
				break;
			}
		}
		return ErrMsg;

	}

	public void VerifyCookieFuntionality(String environment, RemoteWebDriver driver, String Url, String Cookie_Val) throws InterruptedException
	{
		/*String message = null;
		if (environment.toLowerCase().contains("prod") || environment.toLowerCase().contains("performance")||driver.getCurrentUrl().contains("www"))
		{	
			message=InjectCookieFunctionality(environment, driver);
			int counter=0;
			while(!message.contains("PWA Cookie Injected Successfully") && counter<5)
			{
				driver.manage().deleteAllCookies();
				driver.get(Url);
				message=InjectCookieFunctionality(environment, driver);
				counter++;
			}
			if(message.contains("not"))
				message= "Still in WCS Site";
			else
				message= "Cookie Injected";
		}
		return message;*/
		
		
		/*if(Cookie_Val.toLowerCase().contains("mfe")) {
			try {
				String value = getValueOfCookieNamed(driver, "route");	
				String Cookieset="regular";
				if(!value.equals("route cookie is not available")) {
				System.out.println("Current Cookie " + value);	
				int counter=0;
			if (!value.equals(Cookieset)) {
				do {
				counter++;
				deleteCookieNamed(driver, "route");
				Cookie ck = new Cookie("route", Cookieset);
				driver.manage().addCookie(ck);			
				driver.navigate().refresh();
				value= (driver, "route");
				System.out.println("Modified  Cookie " +value);
				}while(!value.equals(Cookieset)&& counter<10);
				}
				}
										
			}
			catch (Exception ex) {
				System.out.println("route cookie is not available");
				String Cookieset="regular";
				Cookie ck = new Cookie("route", Cookieset);
				driver.manage().addCookie(ck);			
				driver.navigate().refresh();
				String val= getValueOfCookieNamed(driver, "route");
				System.out.println("Added  Cookie " +val);
			}

		}
		
		
		else if(Cookie_Val.toLowerCase().contains("ui") ||  Cookie_Val.trim().equals("")){
			String Cookieset = "ui";		
		
		try {
			String value = getValueOfCookieNamed(driver, "route");	
			if(!value.equals("route cookie is not available") && !value.contains("sci")) {
			System.out.println("Current Cookie " + value);	
			int counter=0;
		if (!value.equals(Cookie_Val) && !value.contains("ui") ){
			do {
			counter++;
			deleteCookieNamed(driver, "route");
			Cookie ck = new Cookie("route", Cookieset);
			driver.manage().addCookie(ck);		
			driver.navigate().refresh();
			value= getValueOfCookieNamed(driver, "route");
			System.out.println("Modified  Cookie " +value);
			}while(!value.equals(Cookieset)&& counter<10);
			}
			}
									
		}
		catch (Exception ex) {
			System.out.println("route cookie is not available");
			Cookieset="ui";
			Cookie ck = new Cookie("route", Cookieset);
			driver.manage().addCookie(ck);			
			driver.navigate().refresh();
			String val= getValueOfCookieNamed(driver, "route");
			System.out.println("Added  Cookie " +val);
			}
		}
		
		return "";*/ 
		if(Cookie_Val.toLowerCase().equals("universal")) {
		((JavascriptExecutor) driver).executeScript("document.dispatchEvent(new CustomEvent('optimizeTestEvent', {detail: { 'checkout': 'universal'}}));");
		System.out.println("Universal running");

		}
	else if(Cookie_Val.toLowerCase().equals("non-universal") || Cookie_Val.toLowerCase().equals("nonuniversal")) {
		((JavascriptExecutor) driver).executeScript("document.dispatchEvent(new CustomEvent('optimizeTestEvent', {detail: { 'checkout': ''}}));");
		System.out.println("Non-Universal running");
	}
		//((JavascriptExecutor) driver).executeScript("document.dispatchEvent(new CustomEvent('optimizeTestEvent', {detail: { 'checkout': 'universal'}}));");

	}
	
	public String SetPreviewcookie(String Brands,String environment, RemoteWebDriver driver, String Url, String MFE_Cookie_Val) throws InterruptedException
	{
		
		if(MFE_Cookie_Val.toLowerCase().contains("preview")) {
				try {
			 		System.out.println("Inside Block try-preview");
			 		String value = getValueOfCookieNamed(driver, "route");	
			 		//String Cookieset="regular";
			 		if(!value.equals("route cookie is not available")) {
			 		System.out.println("Current Cookie " + value);	
			 		int counter=0;
			 	if (!value.equals(MFE_Cookie_Val)) {
			 		do {
			 		counter++;
			 		deleteCookieNamed(driver, "route");
			 		Cookie ck = new Cookie("route", MFE_Cookie_Val);
			 		driver.manage().addCookie(ck);			
			 		driver.navigate().refresh();
			 		value= getValueOfCookieNamed(driver, "route");
			 		System.out.println("Modified  Cookie " +value);
			 		}while(!value.equals(MFE_Cookie_Val)&& counter<10);
			 		}
			 		}
											
			}
			 	catch (Exception ex) {
			 		System.out.println("Inside Block catch -preview");
			 		System.out.println("route cookie is not available");
			 		//String Cookieset="regular";
			 		Cookie ck = new Cookie("route", MFE_Cookie_Val);
			 		driver.manage().addCookie(ck);			
			 		driver.navigate().refresh();
			 		String val= getValueOfCookieNamed(driver, "route");
			 		System.out.println("Added  Cookie " +val);
			 	}

			 }
		
		return "";
		
	}
	public String SetMFEcookie(String Brands,String environment, RemoteWebDriver driver, String Url, String MFE_Cookie_Val) throws InterruptedException
	{
		
		if(MFE_Cookie_Val.toLowerCase().contains("preview")) {
			SetPreviewcookie(Brands,environment, driver, gvar.getUrl(),MFE_Cookie_Val);
		}
	//if(environment.equals("uat1")||environment.equals("uat-1")) 
	// System.out.println("Inside Block");
	
	// if (Brands.equals("HD") || Brands.equals("WLF") || Brands.equals("STY"))
	// 	MFE_Cookie_Val="regular";
	
	// if(gvar.getTCName().contains("SCI"))
	// 	MFE_Cookie_Val="sci_ui";
	
	// if(MFE_Cookie_Val.toLowerCase().contains("regular")) {
	// 	try {
	// 		System.out.println("Inside Block try-regular");
	// 		String value = getValueOfCookieNamed(driver, "route");	
	// 		//String Cookieset="regular";
	// 		if(!value.equals("route cookie is not available")) {
	// 		System.out.println("Current Cookie " + value);	
	// 		int counter=0;
	// 	if (!value.equals(MFE_Cookie_Val)) {
	// 		do {
	// 		counter++;
	// 		deleteCookieNamed(driver, "route");
	// 		Cookie ck = new Cookie("route", MFE_Cookie_Val);
	// 		driver.manage().addCookie(ck);			
	// 		driver.navigate().refresh();
	// 		value= getValueOfCookieNamed(driver, "route");
	// 		System.out.println("Modified  Cookie " +value);
	// 		}while(!value.equals(MFE_Cookie_Val)&& counter<10);
	// 		}
	// 		}
									
	// 	}
	// 	catch (Exception ex) {
	// 		System.out.println("Inside Block catch -regular");
	// 		System.out.println("route cookie is not available");
	// 		//String Cookieset="regular";
	// 		Cookie ck = new Cookie("route", MFE_Cookie_Val);
	// 		driver.manage().addCookie(ck);			
	// 		driver.navigate().refresh();
	// 		String val= getValueOfCookieNamed(driver, "route");
	// 		System.out.println("Added  Cookie " +val);
	// 	}

	// }
	
	
	// else if(MFE_Cookie_Val.toLowerCase().contains("ui") ||  MFE_Cookie_Val.trim().equals("")){
	// 	//String Cookieset = "ui";		
	
	// try {
	// 	System.out.println("Inside Block try ui");
	// 	String value = getValueOfCookieNamed(driver, "route");	
	// 	if(!value.equals("route cookie is not available") && !value.contains("sci")) {
	// 	System.out.println("Current Cookie " + value);	
	// 	int counter=0;
	// if (!value.equals(MFE_Cookie_Val) && !value.contains("ui") ){
	// 	do {
	// 	counter++;
	// 	deleteCookieNamed(driver, "route");
	// 	Cookie ck = new Cookie("route", MFE_Cookie_Val);
	// 	driver.manage().addCookie(ck);		
	// 	driver.navigate().refresh();
	// 	value= getValueOfCookieNamed(driver, "route");
	// 	System.out.println("Modified  Cookie " +value);
	// 	}while(!value.equals(MFE_Cookie_Val)&& counter<10);
	// 	}
	// 	}
								
	// }
	// catch (Exception ex) {
	// 	System.out.println("Inside Block catch ui");
	// 	System.out.println("route cookie is not available");
	// 	MFE_Cookie_Val="ui";
	// 	Cookie ck = new Cookie("route", MFE_Cookie_Val);
	// 	driver.manage().addCookie(ck);			
	// 	driver.navigate().refresh();
	// 	String val= getValueOfCookieNamed(driver, "route");
	// 	System.out.println("Added  Cookie " +val);
	// 	}
	// }
	// else if(MFE_Cookie_Val.toLowerCase().contains("sci")) {
	// 	if(MFE_Cookie_Val.toLowerCase().equals("sci")|| MFE_Cookie_Val.toLowerCase().equals("sci-ui"))
	// 		MFE_Cookie_Val="sci_ui";
	// 	try {
	// 		System.out.println("Inside Block try-sci");
	// 		String value = getValueOfCookieNamed(driver, "route");	
	// 		//String Cookieset="regular";
	// 		if(!value.equals("route cookie is not available")) {
	// 		System.out.println("Current Cookie " + value);	
	// 		int counter=0;
	// 	if (!value.equals(MFE_Cookie_Val)) {
	// 		do {
	// 		counter++;
	// 		deleteCookieNamed(driver, "route");
	// 		Cookie ck = new Cookie("route", MFE_Cookie_Val);
	// 		driver.manage().addCookie(ck);			
	// 		driver.navigate().refresh();
	// 		value= getValueOfCookieNamed(driver, "route");
	// 		System.out.println("Modified  Cookie " +value);
	// 		}while(!value.equals(MFE_Cookie_Val)&& counter<10);
	// 		}
	// 		}
									
	// 	}
	// 	catch (Exception ex) {
	// 		System.out.println("Inside Block catch -sci");
	// 		System.out.println("route cookie is not available");
	// 		//String Cookieset="regular";
	// 		Cookie ck = new Cookie("route", MFE_Cookie_Val);
	// 		driver.manage().addCookie(ck);			
	// 		driver.navigate().refresh();
	// 		String val= getValueOfCookieNamed(driver, "route");
	// 		System.out.println("Added  Cookie " +val);
	// 	}

	// }
	
		
	return "";
		
	}
	public String SetGDPRcookie(String environment, RemoteWebDriver driver, String Url, String Cookie_Val, String Country) throws InterruptedException
	{
		
		if(!Country.equals("USA")) {
		
		try {
			String value = getValueOfCookieNamed(driver, "gdpr");	
			if(!value.equals(Country)) {
			System.out.println("Current Cookie " + value);	
			int counter=0;
		if (!value.equals(Country)) {
			do {
			counter++;
			deleteCookieNamed(driver, "gdpr");
			Cookie ck = new Cookie("gdpr", Country);
			driver.manage().addCookie(ck);		
			driver.navigate().refresh();
			value= getValueOfCookieNamed(driver, "gdpr");
			System.out.println("Modified  Cookie " +value);
			if(value.equals(Country))
				break;
			}while(counter<10);
			}
			}
									
		}
		catch (Exception ex) {
			System.out.println("gdpr cookie is not available");
			System.out.println("gdpr cookie added");
			Cookie ck = new Cookie("gdpr", Country);
			driver.manage().addCookie(ck);
			driver.navigate().refresh();
			System.out.println(getValueOfCookieNamed(driver, "gdpr"));	
		
		}
		}
		
		
		return "";
	}

	public String InjectCookieFunctionality(String environment, RemoteWebDriver driver) throws InterruptedException
	{

		if (driver.getCurrentUrl().contains("pwa.www") && environment.toLowerCase().contains("prod")) 
		{
			//				System.out.println("PWA url");
			return "Redirected to PWA url";

		} 
		else 
		{
			System.out.println(driver.getCurrentUrl());


			if (getValueOfCookieNamed(driver, "cellid").equals("pwa_gcp"))
			{
				return "PWA Cookie Injected Successfully By default";
			}
			else
			{

				deleteCookieNamed(driver, "cellid");
				deleteCookieNamed(driver, "destination");

				addCookie(driver, "cellid", "pwa_gcp");
				addCookie(driver, "destination", "pwa_gcp");

				driver.navigate().refresh();
				driver.navigate().refresh();
				if (getValueOfCookieNamed(driver, "cellid").equals("pwa_gcp"))
				{
					return "PWA Cookie Injected Successfully";
				}

				else
				{

//					HomePage Homepage= new HomePage(driver, gvar);
//					if(Homepage.wcsSign.size() == 0) 
					{
						System.out.println("User still in WCS Site");
						driver.navigate().refresh();
//						if(Homepage.wcsSign.size()==0)
							return "PWA Cookie Injected Successfully";
//						else
//							return "Cookie not injected";
					}
//					else
//						return "PWA Cookie Injected Successfully";
				}
			}
		}

	}

	public void WriteToExtentReport(String ResultPath,  String Content, String environment) throws Exception {// Created

		ExtentReports Report;
		ExtentTest ReportTestCase;
		ReportTestCase = null;
		DateTimeFormatter	dtf= DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");
		LocalDateTime now = LocalDateTime.now();
		String date;
		date=dtf.format(now);

		boolean lockAcquired = lock.tryLock(120, TimeUnit.SECONDS);
		tryagain: if (lockAcquired) {

			try {

				Report = new ExtentReports(ResultPath + "/PCIComplianceReport_"+ environment +"_" + gvar.getBrand()+"_" +date+".html",true);
				ReportTestCase = Report.startTest(" PCI Compliance Report- " + environment +"-" +gvar.getBrand());			
				String[] reportarray = Content.split("\\*"); 
				for (int i=0; i<reportarray.length;i++) {
					if(reportarray[i].contains("Failed"))
						ReportTestCase.log(LogStatus.FAIL,  reportarray[i] + "<br/>");
					else						
						ReportTestCase.log(LogStatus.INFO,  reportarray[i] + "<br/>");			


				}
				if(Content.contains("Failed"))
					ReportTestCase.log(LogStatus.FAIL, "Snapshot below: " + ReportTestCase.addScreenCapture(ResultPath  +"/" + gvar.getBrand() + "-SigninPage_Privacy.jpg"));			
				else
					ReportTestCase.log(LogStatus.PASS, "Snapshot below: " + ReportTestCase.addScreenCapture(ResultPath   +"/" + gvar.getBrand() + "-SigninPage_Terms.jpg"));



				Report.endTest(ReportTestCase);
				Report.flush();

			}catch (Exception e) {
				// TODO: handle exception
				System.out.println("Exception" +e);
			} 
			finally {
				lock.unlock();
			}
		} else {
			System.out.println("Lock acquired while trying to write");
			System.out.println("On hold" + lock.getHoldCount());
			System.out.println("Queue Length" + lock.getQueueLength());
			break tryagain;
		}
	}

	public static void UpdateResults(String sessionId, String key,String value, String USERNAME, String ACCESS_KEY) throws JSONException, ClientProtocolException, IOException {
		//created by Nishant @ 24 June 2019
		SauceREST saucerest = new SauceREST(USERNAME, ACCESS_KEY);
		Map<String, Object> updates = new HashMap<String, Object>();
		updates.put(key, value);
		saucerest.updateJobInfo(sessionId, updates);
	}

	public static String GetValueFromLocalStorage(RemoteWebDriver driver,String key)
	{
		JavascriptExecutor js = ((JavascriptExecutor)driver);
		return (String) js.executeScript(String.format("return window.localStorage.getItem('%s');", key));

	}
}
