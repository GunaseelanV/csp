/**
 * @author cnishant/Devi - 15th Oct 2018
 *
 */
package Helper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class GVar
{

	public void setRowData(ArrayList<String> rowData)
	{
		setScenario(rowData.get(1));
		setsrnumber(rowData.get(2));
		setPlatform(rowData.get(3));
		setRunStatus(rowData.get(4));
		setTCName(rowData.get(6));
		setUserType(rowData.get(7));
		setSignInEmail(rowData.get(8));
		setSignInPswd(rowData.get(9));
		setTestScenario(rowData.get(2));
		setBrand(rowData.get(11));
		setUrl(rowData.get(10));
		setLineItemCount(rowData.get(13));
		setProductCategory(rowData.get(14));
		setSku(rowData.get(15));
		setQty(rowData.get(16));
		setTitle(rowData.get(21));
		setFName(rowData.get(22));
		setLName(rowData.get(23));
		setLocation(rowData.get(24));
		setLocationName(rowData.get(25));
		/*if(getBrand().equals("FBQ"))
		{						
			setAddLn1("23227 Mercantile Pkwy");
			setAddLn2("Suite A5");
			setCity("Katy");
			setState("Texas");
			setZip("77449");
		}
		else { */
		setAddLn1(rowData.get(26));
		setAddLn2(rowData.get(27));
		setCity(rowData.get(28));
		setState(rowData.get(29));
		setZip(rowData.get(30));
	//	}
		setExpectedTaxRate(rowData.get(31));
		setTelephone(rowData.get(32));
		setUseAsMyBillAdd(rowData.get(33));
		setDeliveryMethod(rowData.get(34));
		setDate("");
		setComment("");
		setGiftMszOption(rowData.get(37));
		setGiftMsz(rowData.get(38));
		setWrapupOption(rowData.get(39));
		setBillEmail(rowData.get(40));
		setBillConfemail(rowData.get(40));
		setPaymentType(rowData.get(44));
		setPromoCode(rowData.get(42));

		setApplyPromo(rowData.get(41));
		setGC(rowData.get(45));
		setCCNum(rowData.get(50));
		setCC_ExpMnth(rowData.get(51));
		setCC_ExpYr(rowData.get(52));
		setCC_Name(rowData.get(53));
		setCC_SecCode(rowData.get(54));
		setOrderType(rowData.get(21));

		setBillFName("Test");
		setBillLName("Auto");
		setBillComp("1800Flowers");
		setBillAddLn1("212 halsey avenue");
		setBillAddLn2("Suit 500");
		setBillCity("Jericho");
		setBillState("New York");
		setBillZip("11753");
		setBillCountry("United States");
		setBillTelephone("5163126010");
		setBuild("");
		setJobName("");
		setBrowserVersion("");
	}
	
	

	private static  ReentrantLock lock=new ReentrantLock();
	private String ResultPath;public void setResultPath(String path){this.ResultPath=path;}public String getResultPath(){return this.ResultPath;}
	private String OrderScreenshotPath;public void setOrderScreenshotPath(String path){this.OrderScreenshotPath=path;}public String getOrderScreenshotPath(){return this.OrderScreenshotPath;}

	private String RunStatus;
	public void setRunStatus(String runstatus)	{this.RunStatus=runstatus;}
	public String getRunStatus() {return this.RunStatus;}

	private String DataFilePath;
	public void setDataFilePath(String DataFile) {this.DataFilePath=DataFile;}
	public String getDataFilePath() {return this.DataFilePath;}

	private String Browser;
	public void setBrowser(String browser)  {this.Browser=browser;}
	public String getBrowser() {return this.Browser;}
	private String Scenario;
	public void setScenario(String scenario)  {this.Scenario=scenario;}
	public String getScenario() {return this.Scenario;}

	private String Comment ;public void setComment(String Comment) {this.Comment=Comment;}
	public String getComment() {return this.Comment;}
	
	
	
	private String SrNumber;public void setsrnumber(String srNumber) {this.SrNumber=srNumber;}
	public String getSrNumber() {return this.SrNumber;}
	private String TCName;public void setTCName(String tcname){this.TCName=tcname;}	public String getTCName(){return this.TCName;}

	private String TestScenario;public void setTestScenario(String testScenario){this.TestScenario=testScenario;}	public String getTestScenario(){return this.TestScenario;}

	private String Brand;public void setBrand(String brand){this.Brand=brand;}public String getBrand(){return this.Brand;}

	private String Url;public void setUrl(String url){this.Url=url;}public String getUrl(){return this.Url;}
	
	private String cspUrl;public void setcspUrl(String cspurl){this.Url=cspurl;}public String launchCSPURLNewTab(){return this.cspUrl;}

	private String Platform;public void setPlatform(String Platform) {this.Platform=Platform;}public String getPlatform() {return this.Platform;}

	private String LineItemCount;public void setLineItemCount(String lineItemCnt) {this.LineItemCount=lineItemCnt;}public String getLineItemCount() {return LineItemCount;}

	private int LineItem;public void setLineItem(int LineItem){this.LineItem=LineItem;}public int getLineItem(){return this.LineItem;}

	private String Sku;public void setSku(String sku) {this.Sku=sku;}public String getSku() {return this.Sku;}

	private String Qty;public void setQty(String Qty){this.Qty=Qty;}public String getQty(){return this.Qty;}

	private String ProductCategory;public void setProductCategory(String ProductCategory) {this.ProductCategory=ProductCategory;}public String getProductCategory() {return this.ProductCategory;}

	private String ProductPrice;public void setProductPrice(String productPrice) {this.ProductPrice=productPrice;}public String getProductPrice() {return this.ProductPrice;}

	private String Title;public void setTitle(String title) {this.Title=title;}public String getTitle() {return this.Title;}

	private String FName;public void setFName(String fname) {this.FName=fname;}public String getFName() {return this.FName;}

	private String LName;public void setLName(String lname) {this.LName=lname;}public String getLName() {return this.LName;}

	private String organizationName;public void setorganizationName(String orgname) {this.organizationName=orgname;}public String getorganizationName() {return this.organizationName;}

	private String AddLn1;public void setAddLn1(String adLn1) {this.AddLn1=adLn1;}public String getAddLn1() {return this.AddLn1;}

	private String AddLn2;public void setAddLn2(String adLn2) {this.AddLn2=adLn2;}public String getAddLn2() {return this.AddLn2;}

	private String City;public void setCity(String city) {this.City=city;}public String getCity() {return this.City;}

	private String State;public void setState(String state) {this.State=state;}public String getState() {return this.State;}

	private String Country;public void setCountry(String country) {this.Country=country;}public String getCountry() {return this.Country;}

	private String Telephone;public void setTelephone(String telephone) {this.Telephone=telephone;}public String getTelephone() {return this.Telephone;}

	private String ReciEmail;public void setReciEmail(String email) {this.ReciEmail=email;}public String getReciEmail() {return this.ReciEmail;}

	private String UseAsMyBillAdd;public void setUseAsMyBillAdd(String uamba) {this.UseAsMyBillAdd=uamba;}	public String getUseAsMyBillAdd() {return this.UseAsMyBillAdd;}

	private String Zip;	public void setZip(String zip) {this.Zip=zip;}public String getZip() {return this.Zip;}

	private String Build; public void setBuild(String build) {this.Build=build;} public String getBuild() {return this.Build;}
	private String JobName; public void setJobName(String jobName) {this.JobName=jobName;} public String getJobName() {return this.JobName;}
	private String BrowserVersion; public void setBrowserVersion(String browserVersion) {this.BrowserVersion=browserVersion;} public String getBrowserVersion() {return this.BrowserVersion;}

	private String ExpectedTaxRate;	
	public void setExpectedTaxRate(String string) {
		this.ExpectedTaxRate=ExpectedTaxRate;
		// TODO Auto-generated method stub
		
	}
	public String getExpectedTaxRate() {return this.ExpectedTaxRate;}

	
	
	private String Location;public void setLocation(String loc) {this.Location=loc;}public String getLocation() {return this.Location;}


	private String LocationName;public void setLocationName(String loc) {this.LocationName=loc;}public String getLocationName() {return this.LocationName;}

	
	private String DeliveryMethod;	public void setDeliveryMethod(String dmethod){this.DeliveryMethod=dmethod;}	public String getDeliveryMethod(){return this.DeliveryMethod;}

	private String Date;public void setDate(String date) {this.Date=date;}public String getDate() {return this.Date;}

	private String GiftMszOption;public void setGiftMszOption(String GiftMszOption) {this.GiftMszOption=GiftMszOption;}public String getGiftMszOption() {return this.GiftMszOption;}

	private String GiftMsz;public void setGiftMsz(String GiftMsz) {this.GiftMsz=GiftMsz;}public String getGiftMsz() {return this.GiftMsz;}

	private String WrapupOption;public void setWrapupOption(String uamba) {this.WrapupOption=uamba;}public String getWrapupOption() {return this.WrapupOption;}

	private String SignInEmail;public void setSignInEmail(String Email) {this.SignInEmail=Email;} public String getSignInEmail() {return this.SignInEmail;}

	private String SignInPswd;public void setSignInPswd(String Password) {this.SignInPswd=Password;}public String getSignInPswd() {return this.SignInPswd;}

	private String UserType;public void setUserType(String Email) {this.UserType=Email;} public String getUserType() {return this.UserType;}

	private String PaymentType; public void setPaymentType(String payment) {this.PaymentType=payment;}	public String getPaymentType() {return this.PaymentType;}

	private String ApplyPromo; public void setApplyPromo(String Applypromo) {this.ApplyPromo=Applypromo;}	public String getApplyPromo() {return this.ApplyPromo;}

	
	private String OrderType; public void setOrderType(String ordertype) {this.OrderType=ordertype;}	public String getOrderType() {return this.OrderType;}

	private String BillFName;public void setBillFName(String billfname) {this.BillFName=billfname;} public String getBillFName() {return this.BillFName;}

	private String BillLName;public void setBillLName(String billlname) {this.BillLName=billlname;} public String getBillLName() {return this.BillLName;}

	private String BillComp;public void setBillComp(String billcomp) {this.BillComp=billcomp;} public String getBillComp() {return this.BillComp;}

	private String BillAddLn1;public void setBillAddLn1(String adLn1) {this.BillAddLn1=adLn1;}public String getBillAddLn1() {return this.BillAddLn1;}

	private String BillAddLn2;public void setBillAddLn2(String adLn2) {this.BillAddLn2=adLn2;}public String getBillAddLn2() {return this.BillAddLn2;}

	private String BillCity;public void setBillCity(String city) {this.BillCity=city;}public String getBillCity() {return this.BillCity;}

	private String BillState;public void setBillState(String state) {this.BillState=state;}public String getBillState() {return this.BillState;}

	private String BillZip; public void setBillZip(String zip) {this.BillZip=zip;}public String getBillZip() {return this.BillZip;}

	private String BillCountry;public void setBillCountry(String country) {this.BillCountry=country;}public String getBillCountry() {return this.BillCountry;}

	private String BillTelephone;public void setBillTelephone(String telephone) {this.BillTelephone=telephone;}public String getBillTelephone() {return this.BillTelephone;}

	private String BillEmail;public void setBillEmail(String ccnum) {this.BillEmail=ccnum;}public String getBillEmail() {return this.BillEmail;}

	private String BillConfemail;public void setBillConfemail(String billconfemail) {this.BillConfemail=billconfemail;}public String getBillConfemail() {return this.BillConfemail;}

	private String PromoCode;public void setPromoCode(String promo) {this.PromoCode=promo;}public String getPromoCode() {return this.PromoCode;}

	private String GC;public void setGC(String gc) {this.GC=gc;}public String getGC() {return this.GC;}

	private String CCNum;public void setCCNum(String ccnumb) {this.CCNum=ccnumb;}public String getCCNum() {return this.CCNum;}

	private String CC_ExpMnth;public void setCC_ExpMnth(String expmnth) {this.CC_ExpMnth=expmnth;}public String getCC_ExpMnth() {return this.CC_ExpMnth;}

	private String CC_ExpYr;public void setCC_ExpYr(String expyr) {this.CC_ExpYr=expyr;}public String getCC_ExpYr() {return this.CC_ExpYr;}

	private String CC_Name;public void setCC_Name(String ccname) {this.CC_Name=ccname;}public String getCC_Name() {return this.CC_Name;}

	private String CC_SecCode;public void setCC_SecCode(String secCode) {this.CC_SecCode=secCode;}public String getCC_SecCode() {return this.CC_SecCode;}

	public Map<String,String> readXlsRow(File fileName,int RowNumber) throws IOException, InterruptedException
	{
		Map<String,String> TestData = new HashMap<String,String>();
		//File inputWorkbook = new File(fileName+".xls");
		boolean lockAcquired= lock.tryLock(120,TimeUnit.SECONDS);
		tryagain:
			if(lockAcquired) {
				try {
					try
					{
						if(fileName.exists())
						{
							Workbook w = Workbook.getWorkbook(fileName);
							Sheet sheet = w.getSheet(0);
							
							TestData.put("TestCaseNumber", sheet.getCell(2, RowNumber).getContents());
							TestData.put("TestCaseName", sheet.getCell(6, RowNumber).getContents());
							TestData.put("Device", sheet.getCell(3, RowNumber).getContents());
							TestData.put("Brand", sheet.getCell(12, RowNumber).getContents());
							TestData.put("Url", sheet.getCell(10, RowNumber).getContents());
							TestData.put("EmailID", sheet.getCell(8, RowNumber).getContents());
							TestData.put("Password", sheet.getCell(9, RowNumber).getContents());							
							TestData.put("BillingFirstName", sheet.getCell(22, RowNumber).getContents());
							TestData.put("BillingLastName", sheet.getCell(23, RowNumber).getContents());
							TestData.put("RecipientFirstName", sheet.getCell(22, RowNumber).getContents());
							TestData.put("RecipientLastName", sheet.getCell(23, RowNumber).getContents());
							TestData.put("RecipientLocationName", sheet.getCell(24, RowNumber).getContents());
							TestData.put("RecipientAddressLineOne", sheet.getCell(26, RowNumber).getContents());
							TestData.put("RecipientAddressLineTwo", sheet.getCell(27, RowNumber).getContents());
							
							TestData.put("RecipientState", sheet.getCell(29, RowNumber).getContents());
							TestData.put("RecipientCity", sheet.getCell(28, RowNumber).getContents());
							TestData.put("RecipientZipcode", sheet.getCell(30, RowNumber).getContents());
							TestData.put("RecipientPhoneNumber", sheet.getCell(32, RowNumber).getContents());
							
						}
						else
							Thread.sleep(1000);
					}
					catch (BiffException e) {e.printStackTrace();}}
				finally{
					lock.unlock();
				}
			}
			else {
				System.out.println("Lock acquired while trying to write");
				System.out.println("On hold"+lock.getHoldCount());
				System.out.println("Queue Length"+lock.getQueueLength());
				break tryagain;
			}
		
		return TestData;
	}
}
