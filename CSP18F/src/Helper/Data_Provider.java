package Helper;

import java.io.FileInputStream;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.*;


public class Data_Provider{
	@DataProvider(name="GU_Track_Orders", parallel = true)
	public static Object[][] dataProFunction1(){
		
		
		return new Object[][]{{"GU","18F", "GPT", "W01003703134090", "11514", "11514"},
							  {"GU","18F", "FPT", "W01003703134092", "11514", "11514"},
							  {"GU","18F", "INTRANSIT", "W01003703158628", "11514", "11514"},
							  {"GU","18F", "DELIVERED", "W01003703134092", "11514", "11514"},
							  {"GU","18F", "MULTILINE", "W01003703134096", "11514", "11514"},
							  // {"GU","18F", "CARDISLE", "W01003702810044", "11753", "11753"},
							  {"GU","18F", "CYO", "W01003702812614", "11514", "11514"},
							  {"GU","18F", "PASSPORTCHECKOUT", "W01003702835780", "11514", "11514"},
							  
							  
							  
							  {"GU","HD", "GPT", "W01003703134090", "11514", "11514"},
							  {"GU","CCO", "FPT", "W01003703134092", "11514", "11514"},
							  {"GU","TPF", "INTRANSIT", "W01003703158628", "11514", "11514"},
							  {"GU","WLF", "DELIVERED", "W01003703134092", "11514", "11514"},
							  {"GU","18B", "MULTILINE", "W01003703134096", "11514", "11514"},
							  {"GU","FBQ", "GPT", "W01003703134090", "11514", "11514"},
							  {"GU","BRY", "FPT", "W01003703134092", "11514", "11514"},
							  {"GU","SCH", "INTRANSIT", "W01003703158628", "11514", "11514"},
							  {"GU","STY", "DELIVERED", "W01003703134092", "11514", "11514"},
							  {"GU","BRY", "GPT", "W01003703134090", "11514", "11514"},
							  };
							  
							//{"18F", "CYO", "W01003702692168",    "11514", "11514"},
							//{"18F", "CANCELED", "W01003702691332",    "33307", "33307"},
							//{"18F", "REPLACED", "W01003702692168",    "11514", "11514"},
							//{"18F", "SUB", "W01003702801368",    "11514", "11514"},
							//{"18F", "REPLACEMENT", "W01003702603922",    "10001", "10001"},};
							
							
	}
	
	
	
	
	
	@DataProvider(name="RU_Track_Orders", parallel = true)
	public static Object[][] dataProFunction2(){

		
		return new Object[][]{{"18F", "GPT","test18F@GPT.com","Testing@123","W01003702968036"},
			                  {"18F", "FPT","test18F@FPT.com","Testing@123","W01003702968042"},
		                  	{"18F", "INTRANSIT","testintransit09@gmail.com","Testing@123","W01003703158730"},
		                  {"18F", "DELIVERED","flowers14@gmail.com","Danger10","W01003702799318"},
		                  {"18F", "MULTILINEITEM","test18F@MULTI.com","Testing@123","W01003703067452"},
		                  
		                  
		                  
		                
		                  
		                  {"18B", "GPT","test18F@GPT.com","Testing@123","W01003702968036"},
		                  {"FBQ", "FPT","test18F@FPT.com","Testing@123","W01003702968042"},
	                  	{"BRY", "INTRANSIT","testintransit09@gmail.com","Testing@123","W01003703158730"},
	                  {"STY", "DELIVERED","flowers14@gmail.com","Danger10","W01003702799318"},
	                  {"SCH", "MULTILINEITEM","test18F@MULTI.com","Testing@123","W01003703067452"},
		                  
			              //{"18F", "CYO","testuser_track@allbrands.com","Testing@123","Ord_No"},
		                  // {"18F", "REPLACEMENT","testuser_track@allbrands.com","Testing@123","Ord_No"},
						  //{"18F", "REPLACED", "W01003702692168",    "11514", "11514"},
						  //{"18F", "SUB", "W01003702801368",    "11514", "11514"},
		                  
							};						
	}
	
	@DataProvider(name="GU_Cancelled_TrackOrder", parallel = true)
	public static Object[][] dataProFunction3(){
		
		return new Object[][]{
							  
							    //with dummy data
							 	{"18F", "CANCELED", "W01003703032732", "11514", "11514"}
							  
							  };
							  
							}
	@DataProvider(name="RU_Cancelled_Track_Orders", parallel = true)
	public static Object[][] dataProFunction4(){

		
		return new Object[][]{
		                  {"18F", "CANCELED","flowers123@gmail.com","Testing@123","W01003702967350"},
		                  
		                  
						};						
	}
	
	@DataProvider(name="GU_Modified_TrackOrder", parallel = true)
	public static Object[][] dataProFunction5(){
		
		return new Object[][]{
							  
							    //with dummy data
							 	{"18F", "MODIFY", "W01003703032732", "11514", "11514"}
							  
							  };
	}
	@DataProvider(name="RU_Modified_TrackOrder", parallel = true)
	public static Object[][] dataProFunction6(){
		
		return new Object[][]{
							  
							    //with dummy data
							 	{"18F", "MODIFY", "flowers123@gmail.com","Testing@123","W01003702967350"}
							  
							  };
	}
	
	
	
	@DataProvider(name="RegistrationData")
	public static Object[][] dataProFunctionExcel(){
		
		Object[][] arrObj = getFromExcel("./resources/RegistrationData.xlsx", "Data");
		return arrObj;
	}
	
	public static String[][] getFromExcel(String FileName, String SheetName) {
		
		String[][]data = null;
		
		try {
		
			FileInputStream fis = new FileInputStream(FileName);
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet sh = wb.getSheet(SheetName);
			XSSFRow row = sh.getRow(0);
			
			int NoRow = sh.getPhysicalNumberOfRows();
			int NoCol = row.getLastCellNum();
			Cell cell;
			data = new String[NoRow-1][NoCol];
			
			for(int i =1; i< NoRow; i++) {
				for(int j = 0; j < NoCol; j++) {
					row = sh.getRow(i);
					cell = row.getCell(j);
					try{data[i-1][j] = cell.getStringCellValue();}
					catch(Exception e){data[i-1][j] = "";}

				}
				
			}
			wb.close();
			
			
		}catch(Exception e) {
			System.out.println("Exception"+ e.toString());
		}
		
		return data;
	}
}
