package Pages;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Helper.GVar;

public class StoreFront {
	
	RemoteWebDriver driver;
	WebDriverWait wait;
	GVar gvar;
	JavascriptExecutor js; 
	Actions action;
	
	public StoreFront(RemoteWebDriver CallingDriver, GVar gvar)
	{
		this.driver = CallingDriver;
		this.gvar = gvar;
		wait = new WebDriverWait(driver, 30);
		PageFactory.initElements(driver, this);
		js = (JavascriptExecutor) driver; 
		action= new Actions(driver);
	}

	@FindBys(@FindBy(how = How.XPATH, using = "//img[@alt='storeFront']"))
	List<WebElement> storeFront;
	@FindBys(@FindBy(how = How.XPATH, using = "//p[text()='Service']"))
	List<WebElement> service;
	@FindBys(@FindBy(how = How.XPATH, using = "//p[text()='Sales']"))
	List<WebElement> sales;
	@FindBys(@FindBy(how = How.XPATH, using = "//p[text()='Experimental']"))
	List<WebElement> experimental;
	@FindBys(@FindBy(how = How.XPATH, using = "//p[text()='Credit Card Delete']"))
	List<WebElement> creditCardDelete;
	@FindBys(@FindBy(how = How.XPATH, using = "//p[text()='Customer Maintenance']"))
	List<WebElement> customerMaintenance;
	@FindBys(@FindBy(how = How.XPATH, using = "//p[text()='Order Search']"))
	List<WebElement> orderSearch;
	@FindBys(@FindBy(how = How.XPATH, using = "//p[text()='and more...']"))
	List<WebElement> more;
	@FindBys(@FindBy(how = How.XPATH, using = "/p[contains(text(),'Coming soon')]"))
	List<WebElement> comingSoon;
	@FindBys(@FindBy(how = How.XPATH, using = "//p[contains(text(),'New features in development')]"))
	List<WebElement> newFeatures;
	@FindBys(@FindBy(how = How.XPATH, using = "//p[text()='Logout']//parent::button"))
	List<WebElement> logOut;
	@FindBys(@FindBy(how = How.XPATH, using = "//p[contains(text(),'Customer Service Portal')]"))
	List<WebElement> homePageTitle;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='Sign In']"))
	List<WebElement> btnWelcomeSignIn;
	
	
	public void verifyService() throws InterruptedException{
		waitForAjax();
		if((service.size()==0 && !service.get(0).isDisplayed())) 
			gvar.setComment(gvar.getComment()+"Service text is not displaying in store front page - Failed");
		if((creditCardDelete.size()==0 && !creditCardDelete.get(0).isDisplayed())) 
			gvar.setComment(gvar.getComment()+"Credit Card Delete text is not displaying in Service menu - Failed");
		if((customerMaintenance.size()==0 && !customerMaintenance.get(0).isDisplayed())) 
			gvar.setComment(gvar.getComment()+"customerMaintenance text is not displaying in Service menu - Failed");
		if((orderSearch.size()==0 && !orderSearch.get(0).isDisplayed())) 
			gvar.setComment(gvar.getComment()+"orderSearch text is not displaying in Service menu - Failed");
		if((more.size()==0 && !more.get(0).isDisplayed())) 
			gvar.setComment(gvar.getComment()+"and more text is not displaying in Service menu - Failed");
	}
	
	public void verifySales() throws InterruptedException{
		if(comingSoon.size()==0) 
			gvar.setComment(gvar.getComment()+"Coming soon... text is not displaying in store front page - Failed");
	}
	
	public void verifyTitle() throws InterruptedException{
		if((comingSoon.size()==0 && !newFeatures.get(0).isDisplayed())) 
			gvar.setComment(gvar.getComment()+"Homepage title is not displaying in store front page - Failed");
	}
	
	public void verifyExperimental() throws InterruptedException{
		if((newFeatures.size()==0 && !newFeatures.get(0).isDisplayed())) 
			gvar.setComment(gvar.getComment()+"New features in development,available for testing text is missing in store front page - Failed");
	}
	
	public void verifyLogoutButton() throws InterruptedException{
		if((logOut.size()==0 && !logOut.get(0).isDisplayed())) 
			gvar.setComment(gvar.getComment()+"Logout button is not displaying in store front page - Failed");
	}
	
	public void verifyLogoutFunction() throws InterruptedException{
		waitForAjax();
		if((logOut.size()!=0 && logOut.get(0).isDisplayed())) {
			isClickableAndClick(logOut.get(0));
			waitForAjax();
			if((btnWelcomeSignIn.size()==0 && !btnWelcomeSignIn.get(0).isDisplayed())) 
				gvar.setComment(gvar.getComment()+"CSP Sign In page is not displaying in csp-ui URL - Failed");
			
			String currentUrl = driver.getCurrentUrl();
			if (!currentUrl.equals("https://uat-1.origin-gcp.18f.tech/csp-ui")) {
				gvar.setComment(gvar.getComment()+"CSP Sign In page URL is Different - Failed");
			}
			
		} 
		else
			gvar.setComment(gvar.getComment()+"Logout button is not displaying in store front page - Failed");
	}
	
	public void selectService() throws InterruptedException{
		waitForAjax();
		if(service.size()!=0 && service.get(0).isDisplayed()) {
			isClickableAndClick(service.get(0));
		}
		else
			gvar.setComment(gvar.getComment()+"Service option is not displaying - Failed");
		}
	
	public void selectSales() throws InterruptedException{
		if(sales.size()!=0 && sales.get(0).isDisplayed()) {
			isClickableAndClick(sales.get(0));
		}
		else
			gvar.setComment(gvar.getComment()+"Sales option is not displaying - Failed");
		}
	
	public void selectExperimental() throws InterruptedException{
		if(experimental.size()!=0 && experimental.get(0).isDisplayed()) {
			isClickableAndClick(experimental.get(0));
		}
		else
			gvar.setComment(gvar.getComment()+"Experimental option is not displaying - Failed");
		}
	
	public void clickLogout() throws InterruptedException{
		if(logOut.size()!=0 && logOut.get(0).isDisplayed()) {
			isClickableAndClick(logOut.get(0));
		}
		else
			gvar.setComment(gvar.getComment()+"Logout button is not is not displaying in Storefront Page - Failed");
		}
	
	public void WaitForElement(WebElement element) throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));
		} 		
		catch (Exception e) {
//			popup.Offer_PopUp();
//			popup.FeedBackPopUp();
			js.executeScript("window.scrollBy(0,200)");
			if (!(gvar.getPlatform().split("_")[0]).equals("IE"))
				wait.until(ExpectedConditions.elementToBeClickable(element));
		}
	}
	
	public void waitForAjax() {

		try {
			WebDriverWait driverWait = new WebDriverWait(driver, 5);

			ExpectedCondition<Boolean> expectation;   
			expectation = new ExpectedCondition<Boolean>() {

				public Boolean apply(WebDriver driverjs) {

					JavascriptExecutor js = (JavascriptExecutor) driverjs;
					return js.executeScript("return((window.jQuery != null) && (jQuery.active === 0))").equals("true");
				}
			};
			driverWait.until(expectation);
		}       
		catch (TimeoutException exTimeout) {

			// fail code
		}
		catch (WebDriverException exWebDriverException) {

			// fail code
		}

	}
	
	public void isClickableAndClick(WebElement element) throws InterruptedException {		
		try {			
			element.click();
		} catch (Exception e) {
//			popup.Offer_PopUp();
//			popup.FeedBackPopUp();
//			popup.CartPopup();
			WaitForElement(element);
			element.click();
		}
	}
	
	public void isClickableAndEnterData(WebElement element, String Data) throws InterruptedException {
		try {
			element.clear();
			element.sendKeys(Data);
		} catch (Exception e) {
//			popup.Offer_PopUp();
//			popup.FeedBackPopUp();
			WaitForElement(element);
			element.clear();
			element.sendKeys(Data);
		}
	}
	
	
}
