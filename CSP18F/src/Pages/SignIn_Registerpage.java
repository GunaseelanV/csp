package Pages;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Helper.GVar;

public class SignIn_Registerpage {
	RemoteWebDriver driver;
	WebDriverWait wait;
	GVar gvar;
	JavascriptExecutor js; 
	Actions action;
	
	public SignIn_Registerpage(RemoteWebDriver CallingDriver, GVar gvar) 
	{
		this.driver = CallingDriver;
		this.gvar = gvar;
		wait = new WebDriverWait(driver, 30);
		PageFactory.initElements(driver, this);
		js = (JavascriptExecutor) driver; 
		action= new Actions(driver);

	}
	
	// Sign In Pop up Window
	@FindBys(@FindBy(how = How.XPATH, using = "//input[@id='username']"))
	List<WebElement> txtUserName;
	@FindBys(@FindBy(how = How.XPATH, using = "//input[@id='password']"))
	List<WebElement> txtPassword;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[@type='submit']"))
	List<WebElement> btnContinue;
	@FindBys(@FindBy(how = How.XPATH, using = "//a[text()='Forgot password?']"))
	List<WebElement> forgotPassword;
	@FindBys(@FindBy(how = How.XPATH, using = "//a[text()='Sign up']"))
	List<WebElement> signUp;
	@FindBys(@FindBy(how = How.XPATH, using = "//img[@id='prompt-logo-center']"))
	List<WebElement> welcomeLogo;
	
	public void Auth0SignIn() throws InterruptedException
	{		
		boolean switchWindow=false;		
		String currentWindow = driver.getWindowHandle();
		waitForAjax();
		for(String winHandle : driver.getWindowHandles()){
			if(!winHandle.equalsIgnoreCase(currentWindow)) {
				driver.switchTo().window(winHandle);
				switchWindow=true;
			}
		}
		
		 if(txtUserName.size()!=0) {	
			WaitForElement(welcomeLogo.get(0));
			WaitForElement(welcomeLogo.get(0));
			wait.until(ExpectedConditions.elementToBeClickable(txtUserName.get(0)));
			isClickableAndEnterData(txtUserName.get(0), gvar.getSignInEmail());
			isClickableAndEnterData(txtPassword.get(0), gvar.getSignInPswd());
			if(btnContinue.get(0).isDisplayed())
			{				
				isClickableAndClick(btnContinue.get(0));	
			
			}
			
			
			
			if(switchWindow)	
				driver.switchTo().window(currentWindow);

//			ValidateLogout();

		} 
		else if(driver.getPageSource().toLowerCase().contains("oops!, something went wrong")) {
			gvar.setComment(gvar.getComment() + " OOPS error in Sign In page - Failed");
			if(switchWindow)	{
				driver.close();
				driver.switchTo().window(currentWindow);
			}
		}
		else {
			gvar.setComment(gvar.getComment() + " Sign in fields are not displayed - Failed");
			if(switchWindow)	
				driver.switchTo().window(currentWindow);
		}

	}
	
	public void WaitForElement(WebElement element) throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));
		} 		
		catch (Exception e) {
//			popup.Offer_PopUp();
//			popup.FeedBackPopUp();
			js.executeScript("window.scrollBy(0,200)");
			if (!(gvar.getPlatform().split("_")[0]).equals("IE"))
				wait.until(ExpectedConditions.elementToBeClickable(element));
		}
	}
	
	public void waitForAjax() {

		try {
			WebDriverWait driverWait = new WebDriverWait(driver, 5);

			ExpectedCondition<Boolean> expectation;   
			expectation = new ExpectedCondition<Boolean>() {

				public Boolean apply(WebDriver driverjs) {

					JavascriptExecutor js = (JavascriptExecutor) driverjs;
					return js.executeScript("return((window.jQuery != null) && (jQuery.active === 0))").equals("true");
				}
			};
			driverWait.until(expectation);
		}       
		catch (TimeoutException exTimeout) {

			// fail code
		}
		catch (WebDriverException exWebDriverException) {

			// fail code
		}

	}
	
	public void isClickableAndClick(WebElement element) throws InterruptedException {		
		try {			
			element.click();
		} catch (Exception e) {
//			popup.Offer_PopUp();
//			popup.FeedBackPopUp();
//			popup.CartPopup();
			WaitForElement(element);
			element.click();
		}
	}
	
	public void isClickableAndEnterData(WebElement element, String Data) throws InterruptedException {
		try {
			element.clear();
			element.sendKeys(Data);
		} catch (Exception e) {
//			popup.Offer_PopUp();
//			popup.FeedBackPopUp();
			WaitForElement(element);
			element.clear();
			element.sendKeys(Data);
		}
	}
	
}
