package Pages;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Helper.GVar;

public class HomePage {
	
	RemoteWebDriver driver;
	WebDriverWait wait;
	GVar gvar;
	JavascriptExecutor js; 
	Actions action;
	
	public HomePage(RemoteWebDriver CallingDriver, GVar gvar)
	{
		this.driver = CallingDriver;
		this.gvar = gvar;
		wait = new WebDriverWait(driver, 30);
		PageFactory.initElements(driver, this);
		js = (JavascriptExecutor) driver; 
		action= new Actions(driver);
	}

	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='Offline']"))
	List<WebElement> statusDropdown;
	@FindBys(@FindBy(how = How.XPATH, using = "//span[text()='Available']"))
	List<WebElement> statusAvailable;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='Save']"))
	List<WebElement> btnStatusSave;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='Accept']"))
	List<WebElement> btnAcceptNewChatOffer;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[@type='button']//img[@alt='Collapse Chat']"))
	List<WebElement> expandChatCSP;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='Decline']"))
	List<WebElement> btnDeclineNewChatOffer;
	
	public void changeStatus() throws InterruptedException{
		int counter=0;
		if(statusDropdown.size()!=0) {
			action.moveToElement(statusDropdown.get(0)).build().perform();
			isClickableAndClick(statusDropdown.get(0));
		}
		else
			gvar.setComment(gvar.getComment()+ "Status dropdown is not displaying in Homepage - Failed");
		waitForAjax();
		if(statusAvailable.size()!=0) {
		isClickableAndClick(statusAvailable.get(0));
		}
		else
			gvar.setComment(gvar.getComment()+ "Available status is missing from Status dropdown in Homepage - Failed");
		if(btnStatusSave.size()!=0) {
		isClickableAndClick(btnStatusSave.get(0));
		Thread.sleep(3000);
		}
		else
			gvar.setComment(gvar.getComment()+ "SAVE button is missing from Status selection Popup in Homepage - Failed");
//		if(btnAcceptNewChatOffer.size()!=0) {
//			isClickableAndClick(btnAcceptNewChatOffer.get(0));
//		}
		
		while(btnDeclineNewChatOffer.size()==0 && btnDeclineNewChatOffer.size()==0 & counter<30)
			counter++;

		if(btnAcceptNewChatOffer.size()!=0)
		{
			while(btnAcceptNewChatOffer.size()!=0) {
				WaitForElement(btnAcceptNewChatOffer.get(0));
				isClickableAndClick(btnAcceptNewChatOffer.get(0));
				waitForAjax();
			}	
		}
	}
	
	public void openServiceChatcsp() throws InterruptedException{
		if(expandChatCSP.size()!=0) {
			isClickableAndClick(expandChatCSP.get(0));	
		}
	}
	
	public void WaitForElement(WebElement element) throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));
		} 		
		catch (Exception e) {

			js.executeScript("window.scrollBy(0,200)");
			if (!(gvar.getPlatform().split("_")[0]).equals("IE"))
				wait.until(ExpectedConditions.elementToBeClickable(element));
		}
	}
	
	public void waitForAjax() {

		try {
			WebDriverWait driverWait = new WebDriverWait(driver, 5);

			ExpectedCondition<Boolean> expectation;   
			expectation = new ExpectedCondition<Boolean>() {

				public Boolean apply(WebDriver driverjs) {

					JavascriptExecutor js = (JavascriptExecutor) driverjs;
					return js.executeScript("return((window.jQuery != null) && (jQuery.active === 0))").equals("true");
				}
			};
			driverWait.until(expectation);
		}       
		catch (TimeoutException exTimeout) {

			// fail code
		}
		catch (WebDriverException exWebDriverException) {

			// fail code
		}

	}
	
	public void isClickableAndClick(WebElement element) throws InterruptedException {		
		try {			
			element.click();
		} catch (Exception e) {
//			popup.Offer_PopUp();
//			popup.FeedBackPopUp();
//			popup.CartPopup();
			WaitForElement(element);
			element.click();
		}
	}
	
	public void isClickableAndEnterData(WebElement element, String Data) throws InterruptedException {
		try {
			element.clear();
			element.sendKeys(Data);
		} catch (Exception e) {
//			popup.Offer_PopUp();
//			popup.FeedBackPopUp();
			WaitForElement(element);
			element.clear();
			element.sendKeys(Data);
		}
	}
	
	public void SelectFromDropDown(WebElement element, String Data) throws InterruptedException {
		try {
			new Select(element).selectByVisibleText(Data);

		} catch (Exception e) {
//			popup.Offer_PopUp();
//			if(!popup.FeedBackPopUp())
//				popup.ReferAFriend();
			new Select(element).selectByVisibleText(Data);
		}
	}
	
}
