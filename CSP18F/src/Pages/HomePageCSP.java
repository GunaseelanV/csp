package Pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Helper.GVar;

public class HomePageCSP {
	
	RemoteWebDriver driver;
	WebDriverWait wait;
	GVar gvar;
	JavascriptExecutor js; 
	Actions action;
	
	
	public HomePageCSP(RemoteWebDriver CallingDriver, GVar gvar)
	{
		this.driver = CallingDriver;
		this.gvar = gvar;
		wait = new WebDriverWait(driver, 30);
		PageFactory.initElements(driver, this);
		js = (JavascriptExecutor) driver; 
		action= new Actions(driver);
		
	}

	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='Offline']"))
	List<WebElement> statusDropdown;
	@FindBys(@FindBy(how = How.XPATH, using = "//span[@class='text-xl inline px-2']"))
	List<WebElement> userStatus;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='Save']"))
	List<WebElement> btnStatusSave;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='Accept']"))
	List<WebElement> btnAcceptNewChatOffer;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[@type='button']//img[@alt='Collapse Chat']"))
	List<WebElement> expandChatCSP;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='Decline']"))
	List<WebElement> btnDeclineNewChatOffer;
	@FindBys(@FindBy(how = How.XPATH, using = "//div[@class='DraftEditor-editorContainer']//div[@aria-label='rdw-editor']//div[@class='public-DraftStyleDefault-block public-DraftStyleDefault-ltr']"))
	List<WebElement> typeAResponse;
	@FindBys(@FindBy(how = How.XPATH, using = "//img[@alt='Send Message']/parent::button"))
	List<WebElement> btnSendResponse;
	@FindBys(@FindBy(how = How.XPATH, using = "//img[@alt='Collapse Chat']/parent::div"))
	List<WebElement> collapsedChat;
	
	//End Chat
	@FindBys(@FindBy(how = How.XPATH, using = "//div[@class='flex h-full flex-row']//button[@type='button']"))
	List<WebElement> upArrow;
	@FindBys(@FindBy(how = How.XPATH, using = "//h1[text()='Transfer or end chat?']"))
	List<WebElement> endChatHeader;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='Transfer']"))
	List<WebElement> btnTransferChat;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='End Chat']"))
	List<WebElement> btnEndChat;
	@FindBys(@FindBy(how = How.XPATH, using = "//p[text()='Wrap Up Conversation']"))
	List<WebElement> wrapUpConversationHeader;
	@FindBys(@FindBy(how = How.XPATH, using = "//div[@class='h-full w-full overflow-hidden']//button[@type='button']"))
	List<WebElement> wrapUpTypes;
	@FindBys(@FindBy(how = How.XPATH, using = "//div[@class='w-full border-b-2 border-gray-300']//button[@type='button']"))
	List<WebElement> wrapUpSubTypes;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='Wrap Up']"))
	List<WebElement> btnWrapUp;
	@FindBys(@FindBy(how = How.XPATH, using = "//div[@class='flex flex-row gap-2']"))
	List<WebElement> activeConversation;
	@FindBys(@FindBy(how = How.XPATH, using = "//img[@alt='profile icon']"))
	List<WebElement> sideBar;
	
	@FindBys(@FindBy(how = How.XPATH, using = "//div[@class='h-full w-full overflow-auto']//div[@class='w-full']"))
	List<WebElement>customerMessages;
	
	public void changeStatus() throws InterruptedException{
		int counter=0;
		if(statusDropdown.size()!=0) {
			action.moveToElement(statusDropdown.get(0)).build().perform();
			isClickableAndClick(statusDropdown.get(0));
		}
		else
			gvar.setComment(gvar.getComment()+ "Status dropdown is not displaying in CSP Homepage - Failed");
		Thread.sleep(1000);
		if(userStatus.size()!=0) {
			System.out.println(userStatus.get(0).getText());
		isClickableAndClick(userStatus.get(0));
		}
		else
			gvar.setComment(gvar.getComment()+ "Available Status is is not displaying from Status dropdown in CSP Homepage - Failed");
		if(btnStatusSave.size()!=0) {
		isClickableAndClick(btnStatusSave.get(0));
		Thread.sleep(1000);
		}
		else
			gvar.setComment(gvar.getComment()+ "SAVE button is is not displaying from Status selection Popup in CSP Homepage - Failed");
		
//		while(btnAcceptNewChatOffer.size()==0 && btnAcceptNewChatOffer.size()==0 & counter<4)
//			counter++;

		if(btnAcceptNewChatOffer.size()!=0 && btnAcceptNewChatOffer.get(0).isDisplayed()) {
			isClickableAndClick(btnAcceptNewChatOffer.get(0));	
		}
		
		
		while(btnAcceptNewChatOffer.size()==0 && btnAcceptNewChatOffer.size()==0 & counter<7)
			counter++;
		if(btnAcceptNewChatOffer.size()!=0)
			while(btnAcceptNewChatOffer.size()!=0) {
				isClickableAndClick(btnAcceptNewChatOffer.get(0));
				Thread.sleep(2000);
			}
		
		if(activeConversation.size()!=0 && activeConversation.get(0).isDisplayed()) {
			for (int i = 0; i <= 5; i++) 
			{
				System.out.println(activeConversation.size());
				action.moveToElement(sideBar.get(0)).build().perform();
				Thread.sleep(2000);
				isClickableAndClick(activeConversation.get(0));
				Thread.sleep(2000);
				if (collapsedChat.size()!=0 && collapsedChat.get(0).isDisplayed()) {
					
					if(expandChatCSP.size()!=0) {
					isClickableAndClick(expandChatCSP.get(0));
					Thread.sleep(1000);
					endChatCSPPage();
					
					}
					else
					gvar.setComment(gvar.getComment()+ "Collapse/Expand button in chat is not displaying from CSP Page - Failed");
									
					}
					else
					gvar.setComment(gvar.getComment()+ "Collapsed chat window is not displaying from CSP Page - Failed");
				
			}
			
			
		
		}
		
//		waitForAjax();
		
		switchTabs(0);
		
	}
	
public void verifyChatCSPHomePage() throws InterruptedException{
	
	Thread.sleep(2000);
	List<WebElement> customerMsgs = driver.findElements(By.xpath("//div[@class='h-full w-full overflow-auto']"));
	
	System.out.println(customerMsgs.size());
	
	for (int i = 0; i < customerMsgs.size(); i++) {
		String customertext = customerMsgs.get(i).getText();
		System.out.println(customertext);
		if(customertext.contains("Customer To Agent")) {
			System.out.println("Customer sent message received by Agent");
		}
		else
			gvar.setComment(gvar.getComment()+ "Customer sent message is not received by Agent chat Popup in CSP Page - Failed");
	}
	
	Thread.sleep(2000);
	
	endChatCSPPage();
	
	switchTabs(0);
	
}
	public void acceptNewChatOffer() throws InterruptedException{
		int counter=0;
		while(btnAcceptNewChatOffer.size()==0 && btnAcceptNewChatOffer.size()==0 & counter<7)
			counter++;
		if((btnAcceptNewChatOffer.size()!=0 && btnAcceptNewChatOffer.get(0).isDisplayed()) &&
				(btnDeclineNewChatOffer.size()!=0 && btnDeclineNewChatOffer.get(0).isDisplayed())	
				)
		{
			while(btnAcceptNewChatOffer.size()!=0) {
//				WaitForElement(btnAcceptNewChatOffer.get(0));
				isClickableAndClick(btnAcceptNewChatOffer.get(0));
				waitForAjax();
			}	
		}
		else
			gvar.setComment(gvar.getComment()+ "Chat ACCEPT/DECLINE Popup is not displaying at CSP Homepage when connecting to Live agent from brand Homepage Chat - Failed");
		
	}
	
	
	public void agentToCustomerMessage() throws InterruptedException, AWTException{
		
		if (collapsedChat.size()!=0 && collapsedChat.get(0).isDisplayed()) {
		
		if(expandChatCSP.size()!=0) {
//			isClickableAndClick(expandChatCSP.get(0));
			
			if(typeAResponse.size()!=0 && typeAResponse.get(0).isDisplayed()) {
				isClickableAndClick(typeAResponse.get(0));
				WebElement agentMessage = driver.findElement(By.xpath("//div[@class='DraftEditor-editorContainer']//div[@aria-label='rdw-editor']"));
				agentMessage.sendKeys("Agent To Customer");
				
				if(btnSendResponse.size()!=0 && btnSendResponse.get(0).isDisplayed()) {
					WebElement buttonSend = driver.findElement(By.xpath("//img[@alt='Send Message']/parent::button"));
					buttonSend.click();
					System.out.println("Clicked on Send button");
				}
				else
					gvar.setComment(gvar.getComment()+ "Response send button is not displaying from Chat Popup in CSP Page - Failed");
				
				switchTabs(0);
				
			}
			else
				gvar.setComment(gvar.getComment()+ "Type a Response section is not displaying from Chat Popup in CSP Page - Failed");
		}
		else
			gvar.setComment(gvar.getComment()+ "Collapse/Expand button in chat is not displaying from CSP Page - Failed");
		
		}
		else
			gvar.setComment(gvar.getComment()+ "Collapsed chat window is not displaying from CSP Page - Failed");
	}
	
	public void endChatCSPPage() throws InterruptedException{
		
		
		
		if(upArrow.size()!=0 && upArrow.get(0).isDisplayed()) {
			isClickableAndClick(upArrow.get(0));
			
		if((btnTransferChat.size()!=0 && btnTransferChat.get(0).isDisplayed())
		&& (btnEndChat.size()!=0 &&  btnEndChat.get(0).isDisplayed())) {
		isClickableAndClick(btnEndChat.get(0));
		}
		else
		gvar.setComment(gvar.getComment()+ "Transfer/End Chat is not displaying in Chat close Popup from CSP Page - Failed");
		
		Thread.sleep(1000);
		
		if(wrapUpConversationHeader.size()!=0 && wrapUpConversationHeader.get(0).isDisplayed()) {
			
		if(wrapUpTypes.size()!=0 && wrapUpTypes.get(0).isDisplayed()) {
			
			isClickableAndClick(wrapUpTypes.get(0));
			Thread.sleep(1000);
			if(wrapUpSubTypes.size()!=0 && wrapUpSubTypes.get(0).isDisplayed()) {
				isClickableAndClick(wrapUpSubTypes.get(2));
			}
			else
				gvar.setComment(gvar.getComment()+ "Wrap up sub Types is not displaying in Chat Popup from CSP Page - Failed");
			
			if(btnWrapUp.size()!=0 && btnWrapUp.get(0).isDisplayed()) {
				isClickableAndClick(btnWrapUp.get(0));
			}
			else
				gvar.setComment(gvar.getComment()+ "Wrap up button is not displaying in Chat Popup from CSP Page - Failed");
			
		}
		else
			gvar.setComment(gvar.getComment()+ "Wrap up Types is not displaying in Chat Popup from CSP Page - Failed");
		
		}
		else
			gvar.setComment(gvar.getComment()+ "Wrap up conversation is not displaying in Chat Popup after clicking End chat - Failed");	
		}
		else
			gvar.setComment(gvar.getComment()+ "UP Arrow is not displaying in Chat Popup from CSP Page - Failed");
	
		action.moveToElement(sideBar.get(0)).build().perform();
		
//		if(activeConversation.size()!=0 && activeConversation.get(0).isDisplayed()) {
//		
//			action.moveToElement(sideBar.get(0)).build().perform();
//		isClickableAndClick(activeConversation.get(0));
//	
//		}
	}
	
	public void switchTabs(int windowTab) throws InterruptedException{
		ArrayList<String> tab = new ArrayList<> (driver.getWindowHandles());
		
		driver.switchTo().window(tab.get(windowTab));
		waitForAjax();
	}
	
	public void WaitForElement(WebElement element) throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));
		} 		
		catch (Exception e) {

			js.executeScript("window.scrollBy(0,200)");
			if (!(gvar.getPlatform().split("_")[0]).equals("IE"))
				wait.until(ExpectedConditions.elementToBeClickable(element));
		}
	}
	
	public void waitForAjax() {

		try {
			WebDriverWait driverWait = new WebDriverWait(driver, 5);

			ExpectedCondition<Boolean> expectation;   
			expectation = new ExpectedCondition<Boolean>() {

				public Boolean apply(WebDriver driverjs) {

					JavascriptExecutor js = (JavascriptExecutor) driverjs;
					return js.executeScript("return((window.jQuery != null) && (jQuery.active === 0))").equals("true");
				}
			};
			driverWait.until(expectation);
		}       
		catch (TimeoutException exTimeout) {

			// fail code
		}
		catch (WebDriverException exWebDriverException) {

			// fail code
		}

	}
	
	public void isClickableAndClick(WebElement element) throws InterruptedException {		
		try {			
			element.click();
		} catch (Exception e) {
//			popup.Offer_PopUp();
//			popup.FeedBackPopUp();
//			popup.CartPopup();
			WaitForElement(element);
			element.click();
		}
	}
	
	public void isClickableAndEnterData(WebElement element, String Data) throws InterruptedException {
		try {
			element.clear();
			element.sendKeys(Data);
		} catch (Exception e) {
//			popup.Offer_PopUp();
//			popup.FeedBackPopUp();
			WaitForElement(element);
			element.clear();
			element.sendKeys(Data);
		}
	}
	
	public void SelectFromDropDown(WebElement element, String Data) throws InterruptedException {
		try {
			new Select(element).selectByVisibleText(Data);

		} catch (Exception e) {
//			popup.Offer_PopUp();
//			if(!popup.FeedBackPopUp())
//				popup.ReferAFriend();
			new Select(element).selectByVisibleText(Data);
		}
	}
	
}
