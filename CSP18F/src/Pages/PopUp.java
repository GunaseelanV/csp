package Pages;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import Helper.GVar;

public class PopUp {
	RemoteWebDriver driver;
	WebDriverWait wait;
	GVar gvar = new GVar();
	Actions build;
	JavascriptExecutor js;


	public PopUp(RemoteWebDriver CallingDriver, GVar gvar) {
		this.driver = CallingDriver;
		this.gvar = gvar;
		wait = new WebDriverWait(driver, 30);
		PageFactory.initElements(driver, this);
		js=(JavascriptExecutor) CallingDriver;
	}

	@FindBys(@FindBy(how=How.XPATH, using="//*[contains(text(),'not interested')]")) List<WebElement> HDClosePopUp;
	@FindBys(@FindBy(how=How.XPATH, using="//*[@title='Close']")) List<WebElement> Fancy_HD_PopUp;
	@FindBys(@FindBy(how=How.XPATH, using="//*[text()='No Thanks']")) List<WebElement> OfferPopup;

	@FindBys(@FindBy(how=How.XPATH, using="//*[text()='No, Thanks' or text()='No, thanks']")) List<WebElement> FeedbackPopUp;

	@FindBys(@FindBy(how=How.XPATH, using="//div[@class='bx-wrap']//a[@data-click='close']")) List<WebElement> Banner;
	@FindBy(how= How.XPATH, using="//a[@href='/']")
	WebElement Logo;
	@FindBys(@FindBy(how= How.XPATH, using="//a[@class='bx-close bx-close-link bx-close-inside']")) List<WebElement> CloseCCOPopup;

		@FindBys(@FindBy(how=How.XPATH, using="//*[@class='yotpo-close-icon']")) List<WebElement> YatpoClose;

	@FindBys(@FindBy(how=How.XPATH, using="//section[contains(@aria-label,'Refer a Friend')]//following-sibling::a[@role='button']")) List<WebElement> TPFReferAFriend;

	@FindBys(@FindBy(how=How.XPATH, using="//img[contains(@src, 'close.png')]")) List<WebElement> berriesClose;
	@FindBys(@FindBy(how=How.XPATH, using="//button[@data-testid='close-button']")) List<WebElement> CartClose;

	@FindBys(@FindBy(how=How.XPATH, using="//div[@class='bx-wrap']//button[@data-click='close']")) List<WebElement> BannerBtn;
	
	@FindBys(@FindBy(how=How.XPATH, using="//button[contains(text(),'not interested')]")) List<WebElement> OfferPOPupVIT;
	
	@FindBys(@FindBy(how=How.XPATH, using="//div[@class='bx-wrap']//a[@data-click='close']//*[name()='svg']")) List<WebElement> TPFHeaderOfferPopup;
	//a[@class='bx-close bx-close-link bx-close-inside']
	@FindBys(@FindBy(how=How.ID, using="onetrust-accept-btn-handler")) List<WebElement> CookieAccept;
	@FindBys(@FindBy(how=How.XPATH, using="//img[contains(@src,'svg-close-btn')]")) List<WebElement> FeedBack_Close;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[@id='truste-consent-button']"))
	List<WebElement> AcceptCookie;
	
	@FindBys(@FindBy(how = How.XPATH, using = "//div[@id='pe_confirm_optin_1']//div[text()='Close']"))
	List<WebElement> NotificationClose;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='Continue shopping']"))
	List<WebElement> btnContinueShopping;
	
	public boolean HDPopUp() throws InterruptedException
	{
		//	wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(Logo));
		//Thread.sleep(5000);		
		boolean clicked=false;
//		try {
//			if(HDClosePopUp.size()!=0)
//			{			
//				for(int i=0;i<HDClosePopUp.size();i++)
//				{
//					if(HDClosePopUp.get(i).isDisplayed()) {
//						HDClosePopUp.get(i).click();
//						clicked=true;
//						break;
//					}
//				}
//			}
//			wait.until(ExpectedConditions.invisibilityOfAllElements(HDClosePopUp));
//		}
//		catch(Exception ex)
//		{
//			if(HDClosePopUp.size()!=0)
//			{			
//				for(int i=0;i<HDClosePopUp.size();i++)
//				{
//					if(HDClosePopUp.get(i).isDisplayed()) {
//						HDClosePopUp.get(i).click();
//						clicked=true;
//						break;
//					}
//				}
//			}	
//		}
		Thread.sleep(3000);
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE).perform();
		Thread.sleep(3000);
		BannerClose();
		return clicked;
	}

	public boolean Offer_PopUp() throws InterruptedException
	{
		//Thread.sleep(5000);
		//wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(Logo));
		boolean clicked=false;
		NotificationBanner();
		if(gvar.getBrand().equals("PMALL"))
			PMallPopUp();
		else if(gvar.getBrand().equals("HD")||gvar.getBrand().equals("18F")|| gvar.getBrand().equals("WLF"))
			
			clicked=HDPopUp();
		 if(gvar.getBrand().equals("TPF") || gvar.getBrand().equals("18F"))
		{
			if(BannerBtn.size()>0 && BannerBtn.get(0).isDisplayed())
			{
				BannerBtn.get(0).click();
				clicked=true;
			}
			
			else 
				clicked = BannerClose();
			if(clicked==true && gvar.getBrand().equals("TPF") ) {
				Thread.sleep(5000);
			}
		}
		 if(gvar.getBrand().equals("VIT")&&  OfferPOPupVIT.size()>0)
		 {
			 if(OfferPOPupVIT.get(0).isDisplayed()) 
			 {
//				 Thread.sleep(3000);
				 OfferPOPupVIT.get(0).click();
					clicked=true;
				 }
			 if(clicked==true && gvar.getBrand().equals("VIT") ) {
					Thread.sleep(5000);
			 }
		 }
		else if(gvar.getBrand().equals("WLF"))
		{
			if(Banner.size()>0) {
				for (int i=0;i<(Banner.size()); i++) {
					if(Banner.get(i).isDisplayed()) {
						Banner.get(i).click();
						clicked=true;
						break;
					}
				}
				/* Commented below as wlf is failing after closing popup
				 * if( Banner.get(0).isDisplayed()) {
					Banner.get(0).click();
					clicked=true;
				}
				*/
			}
				
			
		}
		else if((gvar.getBrand().equals("18B")  || gvar.getBrand().equals("SCH")|| gvar.getBrand().equals("FBQ")) && BannerBtn.size()>0) {
			if(BannerBtn.get(0).isDisplayed()) {
				BannerBtn.get(0).click();
				clicked=true;
			}
		}
		
		else if(OfferPopup.size()!=0 && OfferPopup.get(0).isDisplayed())
		{
			if((gvar.getPlatform().split("_")[0]).equals("SF"))	
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", OfferPopup.get(0));
			else
				OfferPopup.get(0).click();
			clicked=true;
			try {
				wait.until(ExpectedConditions.invisibilityOfAllElements(OfferPopup));
				BannerClose();
			}
			catch(StaleElementReferenceException ex)
			{
				driver.navigate().refresh();
			}			
		}
		if(AcceptCookie.size()!=0 && AcceptCookie.get(0).isDisplayed()  && !gvar.getTCName().split("_")[3].toLowerCase().contains("rcppgacceptcookie"))

		{
			isClickableAndClick(AcceptCookie.get(0));
			System.out.println("Closing Consent Cookie");
			Thread.sleep(2000);
		}
		if((gvar.getBrand().equals("CCO")|| gvar.getBrand().equals("HD")||gvar.getBrand().equals("TPF"))  && CloseCCOPopup.size()>0) {
				if(CloseCCOPopup.get(0).isDisplayed()) {
					CloseCCOPopup.get(0).click();
					clicked=true;
				}
			}
			
		if((gvar.getBrand().equals("HD")) &&  YatpoClose.size()>0) {
			
			if(YatpoClose.get(0).isDisplayed()) {
				YatpoClose.get(0).click();
				clicked=true;
			}
			
		}
		
		/*if((gvar.getBrand().equals("TPF")) &&  TPFHeaderOfferPopup.size()>0)
		{
			if(TPFHeaderOfferPopup.size()!=0) {
				//build.moveToElement(TPFHeaderOfferPopup.get(0)).click().build().perform();
				//TPFHeaderOfferPopup.get(0).click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", TPFHeaderOfferPopup.get(0));
				//isClickableAndClick(TPFHeaderOfferPopup.get(0));	
			}
			
			
		}*/
		
		//				Thread.sleep(3000);
		return clicked;
	}
	
	public void NotificationBanner() throws InterruptedException {
		if((gvar.getBrand().equals("CCO")|| gvar.getBrand().equals("HD") ||gvar.getBrand().equals("18F") || gvar.getBrand().equals("WLF")) ) {
		if(NotificationClose.size()!=0 && NotificationClose.get(0).isDisplayed()) {
			isClickableAndClick(NotificationClose.get(0));
		}
		}
	}

	public boolean BannerClose() throws InterruptedException
	{
		boolean clicked=false;			
		
		try {
			for(int j=0;j<2;j++) {
				if(!clicked) {
					//int size= Banner.size();
					if(Banner.size()!=0)
					{						
						for(int i=0; i< Banner.size();i++) {					
							if(Banner.get(i).isDisplayed()) {								
								isClickableAndClick(Banner.get(i));							
								clicked=true;
							}
						}					
					}
				}
			}
		}
		catch(StaleElementReferenceException ex)
		{
			driver.navigate().refresh();
		}
		catch(IndexOutOfBoundsException ex)
		{
			System.out.println("Element is not displayed");
		}
		return clicked;
	}
	public boolean FeedBackPopUp() throws InterruptedException
	{
		boolean clicked=false;
		//wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(Logo));
		if(FeedbackPopUp.size()!=0)
		{
			for(int i=0;i<FeedbackPopUp.size();i++)
			{
				if(FeedbackPopUp.get(i).isDisplayed()) {
					FeedbackPopUp.get(i).click();
					clicked=true;
					break;
				}
			}
		}
		else if(FeedBack_Close.size()!=0)
		{
			for(int i=0;i<FeedBack_Close.size();i++)
			{
				if(FeedBack_Close.get(i).isDisplayed()) {
					System.out.println("**********************************************************************");
					System.out.println("****************************CLOSING FEEDBACK POP UP "+gvar.getSrNumber());
					System.out.println("**********************************************************************");
					FeedBack_Close.get(i).click();
					clicked=true;
					break;
				}
			}
		}
		return clicked;
	}

	public void ReferAFriend()
	{
		//if(!gvar.getBrand().equals("18B")) {
		try {
			if(TPFReferAFriend.size()!=0) {
				for(int i=0;i<2;i++) {
					if(TPFReferAFriend.size()!=0 && TPFReferAFriend.get(0).isDisplayed()) {
						TPFReferAFriend.get(0).click();		
					}
					else if(TPFReferAFriend.size()>1 && TPFReferAFriend.get(1).isDisplayed())
						TPFReferAFriend.get(1).click();		

				}
				wait.until(ExpectedConditions.invisibilityOfAllElements(TPFReferAFriend));
			}
		}
		catch(Exception ex)
		{
			//driver.navigate().refresh();
		}
		//}
	}
	public void CartPopup() {
		if(CartClose.size()!=0) {
			for(int i=0;i<CartClose.size();i++) {
				if(CartClose.get(i).isDisplayed()) {
					CartClose.get(i).click();
					break;
				}
			}
		}
	}

	public void WaitForElement(WebElement element) throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));
		} 		
		catch (Exception e) {
			Offer_PopUp();
			FeedBackPopUp();
			js.executeScript("window.scrollBy(0,200)");
			if (!(gvar.getPlatform().split("_")[0]).equals("IE"))
				wait.until(ExpectedConditions.elementToBeClickable(element));
		}
	}
	
	public boolean welcomeBackPopUp() throws InterruptedException
	{
		boolean clicked=false;
		//wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(Logo));
		if(btnContinueShopping.size()!=0)
		{
			for(int i=0;i<btnContinueShopping.size();i++)
			{
				if(btnContinueShopping.get(i).isDisplayed()) {
					btnContinueShopping.get(i).click();
					clicked=true;
					break;
				}
			}
		}
		return clicked;
	}
	
	public void PMallPopUp()
	{
		if(Fancy_HD_PopUp.size()!=0 && Fancy_HD_PopUp.get(0).isDisplayed())
			Fancy_HD_PopUp.get(0).click();
		if(CookieAccept.size()!=0 && CookieAccept.get(0).isDisplayed())
			CookieAccept.get(0).click();
	}
	
	
	/*
	 * public void isClickableAndClick(WebElement element) throws
	 * InterruptedException { //
	 * wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.
	 * elementToBeClickable(Logo));
	 * 
	 * try { element.click(); } catch (Exception e) {
	 * System.out.println("element is not clickable..."); } }
	 */
	public void isClickableAndClick(WebElement element) throws InterruptedException {
		//		WaitForElement(Logo);
		try {
			//wait.until(ExpectedConditions.elementToBeClickable(element));
			if((gvar.getPlatform().split("_")[0]).equals("SF"))	
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
			else
				element.click();
		} catch (Exception e) {
			js.executeScript("window.scrollBy(0,200)");
			element.click();
		}
	}
}
