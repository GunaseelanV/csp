package Pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Helper.GVar;

public class HomePageBrands {
	
	RemoteWebDriver driver;
	WebDriverWait wait;
	GVar gvar;
	JavascriptExecutor js; 
	Actions action;
	HomePageCSP hmpg;
	PopUp popup;
	public HomePageBrands(RemoteWebDriver CallingDriver, GVar gvar)
	{
		this.driver = CallingDriver;
		this.gvar = gvar;
		popup=new PopUp(driver,gvar);
		wait = new WebDriverWait(driver, 30);
		PageFactory.initElements(driver, this);
		js = (JavascriptExecutor) driver; 
		action= new Actions(driver);
		hmpg = new HomePageCSP(driver, gvar);
	}

	@FindBys(@FindBy(how = How.XPATH, using = "//button[@data-testid='open-chat-btn']"))
	List<WebElement> btnChatBrandHome;
	@FindBys(@FindBy(how = How.XPATH, using = "//div[@data-testid='chat-header']"))
	List<WebElement> chatHeaderBrandHome;
	@FindBys(@FindBy(how = How.XPATH, using = "//form[@data-testid='chat-input-form']//input[@placeholder='Enter message...']"))
	List<WebElement> chatInputBrandHome;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[@data-testid='chat-send-button']"))
	List<WebElement> btnChatSendBrandHome;
	@FindBys(@FindBy(how = How.XPATH, using = "//span[text()='Virtual Assistant disconnected']"))
	List<WebElement> virtualAssistantMessage;
	@FindBys(@FindBy(how = How.XPATH, using = "//span[text()='Please wait while we transfer you to a live agent.']"))
	List<WebElement> agentMessageBeforeConnect;
	@FindBys(@FindBy(how = How.XPATH, using = "//span[text()='Agent has joined the conversation.']"))
	List<WebElement> agentMessageAfterConnect;
	@FindBys(@FindBy(how = How.XPATH, using = "//span[contains(text(),'All agents are currently busy')]"))
	List<WebElement> agentsAreBusyMsg;
	@FindBys(@FindBy(how = How.XPATH, using = "//span[contains(text(),'No agents are currently available to assist you')]"))
	List<WebElement> noAgentAvailableMsg;
	@FindBys(@FindBy(how = How.XPATH, using = "//span[text()='Agent has joined the conversation.']"))
	List<WebElement> agentJoinedMsg;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='Accept']"))
	List<WebElement> btnAcceptNewChatOffer;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='Decline']"))
	List<WebElement> btnDeclineNewChatOffer;
	@FindBys(@FindBy(how = How.XPATH, using = "//div[@id='chatcontainer']//span[text()='How may I help you today?']"))
	List<WebElement> mayIHelp;
	@FindBys(@FindBy(how = How.XPATH, using = "//span[contains(text(),'Agent To Customer')]"))
	List<WebElement> agentToCustomer;
	@FindBys(@FindBy(how = How.XPATH, using = "//span[contains(text(),'Agent has disconnected.')]"))
	List<WebElement> agentDisconnectedMsg;
	@FindBys(@FindBy(how = How.XPATH, using = "//div[@data-component='reconnectChatButton']"))
	List<WebElement> agentLeftMsg;
	@FindBys(@FindBy(how = How.XPATH, using = "//button[@id='reconnect']"))
	List<WebElement> btnReconnectChat;
	@FindBys(@FindBy(how = How.XPATH, using = "//div[@class='flex flex-row gap-2']"))
	List<WebElement> activeConversation;
	@FindBys(@FindBy(how = How.XPATH, using = "//img[@alt='profile icon']"))
	List<WebElement> sideBar;
	@FindBys(@FindBy(how = How.XPATH, using = "//img[@alt='Collapse Chat']//parent::button"))
	List<WebElement> collapsedChat;
	@FindBys(@FindBy(how = How.XPATH, using = "//img[@alt='Collapse Chat']//parent::button"))
	List<WebElement> expandChatCSP;
	@FindBys(@FindBy(how = How.XPATH, using = "//div[@class='DraftEditor-editorContainer']//div[@aria-label='rdw-editor']//div[@class='public-DraftStyleDefault-block public-DraftStyleDefault-ltr']"))
	List<WebElement> typeAResponse;
	@FindBys(@FindBy(how = How.XPATH, using = "//img[@alt='Send Message']/parent::button"))
	List<WebElement> btnSendResponse;
	
	
	public void openChatBrandHomePage() throws InterruptedException{
		int counter=0;
//		switchTabs(0);
		
		if(btnChatBrandHome.size()!=0 && btnChatBrandHome.get(0).isDisplayed()) {
			isClickableAndClick(btnChatBrandHome.get(0));	
		}
		else
			gvar.setComment(gvar.getComment()+ "Chat option is not displaying in brand Homepage - Failed");
		
		if((chatHeaderBrandHome.size()==0 && !chatHeaderBrandHome.get(0).isDisplayed())) 
			gvar.setComment(gvar.getComment()+"Chat window is not displaying in Brand Homepage  - Failed");
		waitForAjax();
		waitForAjax();
//		WaitForElement(mayIHelp.get(0));
		if(chatInputBrandHome.size()!=0 && chatInputBrandHome.get(0).isDisplayed()) {
			isClickableAndEnterData(chatInputBrandHome.get(0), "agent");
		if(btnChatSendBrandHome.size()!=0 && btnChatSendBrandHome.get(0).isDisplayed()) {
			isClickableAndClick(btnChatSendBrandHome.get(0));
		}
		else {
			gvar.setComment(gvar.getComment()+ "Chat send button is not displaying at chat window in brand Homepage - Failed");
			}
			waitForAjax();
			waitForAjax();
			isClickableAndEnterData(chatInputBrandHome.get(0), "agent");
			if(btnChatSendBrandHome.size()!=0 && btnChatSendBrandHome.get(0).isDisplayed()) {
				isClickableAndClick(btnChatSendBrandHome.get(0));
				waitForAjax();
				WaitForElement(virtualAssistantMessage.get(0));
				if(virtualAssistantMessage.size()!=0 && virtualAssistantMessage.get(0).isDisplayed()) {
					System.out.println("Virtual Assistant disconnected message is displaying");
				}
				else 
					gvar.setComment(gvar.getComment()+ "Virtual Assistant disconnected message is not displaying at chat Popup in brand Homepage - Failed");
					
				if(agentMessageBeforeConnect.size()!=0 && agentMessageBeforeConnect.get(0).isDisplayed()) {
					System.out.println("Please wait while we transfer you to a live agent. message is displaying");
				}
				else
					gvar.setComment(gvar.getComment()+ "Please wait while we transfer you to a live agent is not displaying at chat Popup in brand Homepage - Failed");
				
				if(agentsAreBusyMsg.size()!=0 && agentsAreBusyMsg.get(0).isDisplayed()) {
					System.out.println("All agents are currently busy Please wait for the next available agent");
					gvar.setComment(gvar.getComment()+ "All agents are currently busy Message is displaying at chat Popup in brand Homepage - Failed");
				}
				if(noAgentAvailableMsg.size()!=0 && noAgentAvailableMsg.get(0).isDisplayed()) {
					System.out.println("No agents are currently available to assist you");
					gvar.setComment(gvar.getComment()+ "No agents are currently available to assist you Message is displaying at chat Popup in brand Homepage - Failed");	
				}
				switchTabs(1);
				
				if(btnAcceptNewChatOffer.size()!=0) {
					isClickableAndClick(btnAcceptNewChatOffer.get(0));	
				}
				else
					gvar.setComment(gvar.getComment()+ "Chat ACCEPT Popup is not displaying at CSP Homepage after Live agent connected in brand Homepage Chat - Failed");
				
				while(btnAcceptNewChatOffer.size()==0 && btnAcceptNewChatOffer.size()==0 & counter<3)
					counter++;
				if(btnAcceptNewChatOffer.size()!=0)
					while(btnAcceptNewChatOffer.size()!=0) {
						isClickableAndClick(btnAcceptNewChatOffer.get(0));
						waitForAjax();
					}
				
			switchTabs(0);	
			waitForAjax();
			
			if(agentJoinedMsg.size()!=0 && agentJoinedMsg.get(0).isDisplayed()) {
				System.out.println("Agent has joined the conversation. message is displaying");
			}
			else
				gvar.setComment(gvar.getComment()+ "Agent has joined the conversation. is not displaying at chat Popup in brand Homepage - Failed");
			
			isClickableAndEnterData(chatInputBrandHome.get(0), "Automation Testing");
			isClickableAndClick(btnChatSendBrandHome.get(0));
			
			switchTabs(1);
			
			Thread.sleep(2000);
			
			action.moveToElement(sideBar.get(0)).build().perform();
			
			if(activeConversation.size()!=0 && activeConversation.get(0).isDisplayed()){
		
				for (int i = 0; i < activeConversation.size(); i++) {
					action.moveToElement(sideBar.get(0)).build().perform();
					isClickableAndClick(activeConversation.get(0));
					isClickableAndClick(activeConversation.get(0));
					
		if (collapsedChat.size()!=0 && collapsedChat.get(0).isDisplayed()) {
						
		if(expandChatCSP.size()!=0) {
		isClickableAndClick(expandChatCSP.get(0));
			
		List<WebElement> customerMsgs = driver.findElements(By.xpath("//div[@class='h-full w-full overflow-auto']"));
				
		for (int j = 0; j < customerMsgs.size(); j++) {
			String customertext = customerMsgs.get(j).getText();
			System.out.println(customertext);
			if(!customertext.contains("Automation Testing")) {
				hmpg.endChatCSPPage();
			}
		}
		
		
		
		}
		else
		gvar.setComment(gvar.getComment()+ "Collapse/Expand button in chat is not displaying from CSP Page - Failed");
						
		}
		else
		gvar.setComment(gvar.getComment()+ "Collapsed chat window is not displaying from CSP Page - Failed");
					
		}
					
		}
		else
		gvar.setComment(gvar.getComment()+ "Active conversation is not displaying at sidebar in CSP Page - Failed");
		}
		else
			gvar.setComment(gvar.getComment()+ "Chat send button is not displaying at chat window in brand Homepage - Failed");
		}
		else
			gvar.setComment(gvar.getComment()+ "Chat input field is not displaying in brand Homepage - Failed");
		
	}
	
	public void verifyChatBrandHomePage() throws InterruptedException{
		
		if(agentJoinedMsg.size()!=0 && agentJoinedMsg.get(0).isDisplayed()) {
			System.out.println("Agent  has joined the conversation. Message is displaying in Home page chat Popup in Brand tab");
			
			if(agentToCustomer.size()!=0 && agentToCustomer.get(0).isDisplayed()){
				System.out.println("Agent sent Message is displaying in Home page chat Popup in Brand tab");
			}
			else
				gvar.setComment(gvar.getComment()+ "Agent sent Message is not displaying in Home page chat Popup in Brand tab - Failed");
		}
		else
			gvar.setComment(gvar.getComment()+ "Agent  has joined message is not displaying in Home page chat Popup in Brand tab - Failed");
	}
	
	public void verifyChatDisconnectFromBrandHomePage() throws InterruptedException{
		
		if(agentDisconnectedMsg.size()!=0 && agentDisconnectedMsg.get(0).isDisplayed()) {
			System.out.println("Agent Disconnected Message is displaying in Home page chat Popup in Brand tab");
		}
		else
			gvar.setComment(gvar.getComment()+ "Agent Disconnected Message is not displaying in Home page chat Popup in Brand tab - Failed");
		
		if(agentLeftMsg.size()!=0 && agentLeftMsg.get(0).isDisplayed()) {
			System.out.println("Agent has left Message is displaying in Home page chat Popup in Brand tab");
		}
		else
			gvar.setComment(gvar.getComment()+ "Agent has left Message is not displaying in Home page chat Popup in Brand tab - Failed");
		
		if(btnReconnectChat.size()!=0 && btnReconnectChat.get(0).isDisplayed()) {
			System.out.println("Reconnect Chat button is displaying in Home page chat Popup in Brand tab");
		}
		else
			gvar.setComment(gvar.getComment()+ "Reconnect Chat button is not displaying in Home page chat Popup in Brand tab - Failed");
			
	}
	
	public void customerToAgentMessage() throws InterruptedException{
		int counter = 0;
		if(btnChatBrandHome.size()!=0 && btnChatBrandHome.get(0).isDisplayed()) {
//			isClickableAndClick(btnChatBrandHome.get(0));
			
			if(chatInputBrandHome.size()!=0 && chatInputBrandHome.get(0).isDisplayed()) {
				isClickableAndEnterData(chatInputBrandHome.get(0), "Customer To Agent");
			if(btnChatSendBrandHome.size()!=0 && btnChatSendBrandHome.get(0).isDisplayed()) {
				isClickableAndClick(btnChatSendBrandHome.get(0));
			}
			else 
				gvar.setComment(gvar.getComment()+ "Chat send button is not displaying at chat window in brand Homepage - Failed");
	
			}
			else
				gvar.setComment(gvar.getComment()+ "Chat input field is not displaying in brand Homepage - Failed");
			
		}
		else if(chatHeaderBrandHome.size()!=0 && chatHeaderBrandHome.get(0).isDisplayed()) {
			
			if(chatInputBrandHome.size()!=0 && chatInputBrandHome.get(0).isDisplayed()) {
				isClickableAndEnterData(chatInputBrandHome.get(0), "Customer To Agent");
			if(btnChatSendBrandHome.size()!=0 && btnChatSendBrandHome.get(0).isDisplayed()) {
				isClickableAndClick(btnChatSendBrandHome.get(0));
			}
			else 
				gvar.setComment(gvar.getComment()+ "Chat send button is not displaying at chat window in brand Homepage - Failed");
	
			}
			else
				gvar.setComment(gvar.getComment()+ "Chat input field is not displaying in brand Homepage - Failed");
			
		}
		else
			gvar.setComment(gvar.getComment()+ "Chat option is not displaying in brand Homepage - Failed");
		
		switchTabs(1);
		
	}
	
	public void connectToLiveAgent() throws InterruptedException{
		if(btnChatBrandHome.size()!=0) {
			isClickableAndClick(btnChatBrandHome.get(0));	
		}
		else
			gvar.setComment(gvar.getComment()+ "Chat option is not displaying in brand Homepage - Failed");
		
	}
	
	public void acceptNewChatOffer() throws InterruptedException{
		int counter=0;
		while(btnAcceptNewChatOffer.size()==0 && btnAcceptNewChatOffer.size()==0 & counter<7)
			counter++;
		if((btnAcceptNewChatOffer.size()!=0) && (btnDeclineNewChatOffer.size()!=0))
		{
			
			while(btnAcceptNewChatOffer.size()!=0) {
//				WaitForElement(btnAcceptNewChatOffer.get(0));
				isClickableAndClick(btnAcceptNewChatOffer.get(0));
				waitForAjax();
			}	
		}
		else
			gvar.setComment(gvar.getComment()+ "Chat ACCEPT/DECLINE Popup is not displaying at CSP Homepage when connecting to Live agent from brand Homepage Chat - Failed");
		
	}
	
	public void switchTabs(int windowTab) throws InterruptedException{
		ArrayList<String> tab = new ArrayList<> (driver.getWindowHandles());
		
		driver.switchTo().window(tab.get(windowTab));
//		waitForAjax();
	}
	
	public void WaitForElement(WebElement element) throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));
		} 		
		catch (Exception e) {

			js.executeScript("window.scrollBy(0,200)");
			if (!(gvar.getPlatform().split("_")[0]).equals("IE"))
				wait.until(ExpectedConditions.elementToBeClickable(element));
		}
	}
	
	public void waitForAjax() {

		try {
			WebDriverWait driverWait = new WebDriverWait(driver, 5);

			ExpectedCondition<Boolean> expectation;   
			expectation = new ExpectedCondition<Boolean>() {

				public Boolean apply(WebDriver driverjs) {

					JavascriptExecutor js = (JavascriptExecutor) driverjs;
					return js.executeScript("return((window.jQuery != null) && (jQuery.active === 0))").equals("true");
				}
			};
			driverWait.until(expectation);
		}       
		catch (TimeoutException exTimeout) {

			// fail code
		}
		catch (WebDriverException exWebDriverException) {

			// fail code
		}

	}
	
	public void isClickableAndClick(WebElement element) throws InterruptedException {		
		try {			
			element.click();
		} catch (Exception e) {
//			popup.Offer_PopUp();
//			popup.FeedBackPopUp();
//			popup.CartPopup();
			WaitForElement(element);
			element.click();
		}
	}
	
	public void isClickableAndEnterData(WebElement element, String Data) throws InterruptedException {
		try {
			element.clear();
			element.sendKeys(Data);
		} catch (Exception e) {
//			popup.Offer_PopUp();
//			popup.FeedBackPopUp();
			WaitForElement(element);
			element.clear();
			element.sendKeys(Data);
		}
	}
	
	public void SelectFromDropDown(WebElement element, String Data) throws InterruptedException {
		try {
			new Select(element).selectByVisibleText(Data);

		} catch (Exception e) {
//			popup.Offer_PopUp();
//			if(!popup.FeedBackPopUp())
//				popup.ReferAFriend();
			new Select(element).selectByVisibleText(Data);
		}
	}
	
}
