package Pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Helper.GVar;

public class WelcomeSignInPage {
	
	RemoteWebDriver driver;
	WebDriverWait wait;
	GVar gvar;
	JavascriptExecutor js; 
	Actions action;
	
	public WelcomeSignInPage(RemoteWebDriver CallingDriver, GVar gvar ) {
		this.driver = CallingDriver;
		this.gvar = gvar;
		wait = new WebDriverWait(driver, 30);
		PageFactory.initElements(driver, this);
		js = (JavascriptExecutor) driver; 
		action= new Actions(driver);
	}

	
	//Welcome Sign In Page 
	@FindBys(@FindBy(how = How.XPATH, using = "//button[text()='Sign In']"))
	List<WebElement> btnWelcomeSignIn;
	
	public void welcomeSignIn() throws InterruptedException
	{
	waitForAjax();
	if(btnWelcomeSignIn.size()!=0 && btnWelcomeSignIn.get(0).isDisplayed()) {
		isClickableAndClick(btnWelcomeSignIn.get(0));
		Thread.sleep(2000);
	}
	else
		gvar.setComment(gvar.getComment()+"Welcome Popup Sign In button is not displaying - Failed");
	Thread.sleep(2000);
	
	}
	
	public void launchCSPURLNewTab()throws InterruptedException {
		
		((JavascriptExecutor)driver).executeScript("window.open()");
		
		ArrayList<String> tabs = new ArrayList<> (driver.getWindowHandles());
		
		driver.switchTo().window(tabs.get(1));
		
//		boolean switchWindow=false;		
//		String currentWindow = driver.getWindowHandle();
//		waitForAjax();
//		for(String winHandle : driver.getWindowHandles()){
//			if(!winHandle.equalsIgnoreCase(currentWindow)) {
//				driver.switchTo().window(winHandle);
//				switchWindow=true;
//			}
//		}
		
		
		driver.get("https://uat-1.origin-gcp.18f.tech/csp-ui/");
		
		welcomeSignIn();
		
	}
	
	public void switchTabs(int tabno) throws InterruptedException{
		
		ArrayList<String> tabs = new ArrayList<> (driver.getWindowHandles());
		
		driver.switchTo().window(tabs.get(tabno));
	}
	
	public void WaitForElement(WebElement element) throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));
		} 		
		catch (Exception e) {
//			popup.Offer_PopUp();
//			popup.FeedBackPopUp();
			js.executeScript("window.scrollBy(0,200)");
			if (!(gvar.getPlatform().split("_")[0]).equals("IE"))
				wait.until(ExpectedConditions.elementToBeClickable(element));
		}
	}
	
	public void waitForAjax() {

		try {
			WebDriverWait driverWait = new WebDriverWait(driver, 5);

			ExpectedCondition<Boolean> expectation;   
			expectation = new ExpectedCondition<Boolean>() {

				public Boolean apply(WebDriver driverjs) {

					JavascriptExecutor js = (JavascriptExecutor) driverjs;
					return js.executeScript("return((window.jQuery != null) && (jQuery.active === 0))").equals("true");
				}
			};
			driverWait.until(expectation);
		}       
		catch (TimeoutException exTimeout) {

			// fail code
		}
		catch (WebDriverException exWebDriverException) {

			// fail code
		}

	}
	
	public void isClickableAndClick(WebElement element) throws InterruptedException {		
		try {			
			element.click();
		} catch (Exception e) {
//			popup.Offer_PopUp();
//			popup.FeedBackPopUp();
//			popup.CartPopup();
			WaitForElement(element);
			element.click();
		}
	}
	
	
}
