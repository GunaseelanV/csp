package TestCases;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Helper.GVar;
import Helper.HelperMethods;
import Helper.SetDesiredCapabilities;
import Pages.HomePageCSP;
import Pages.PopUp;
import Pages.HomePageBrands;
import Pages.HomePageCSP;
import Pages.SignIn_Registerpage;
import Pages.StoreFront;
import Pages.WelcomeSignInPage;
//import Pages.HomePage;
//import Pages.PopUp;

public class FunctionalityTestCases {
	
	public static final String USERNAME = "balajisaucelabs";
	public static final String ACCESS_KEY = "70e4d288-e430-4ce9-835d-d00ac26a9266";

	// public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY +
	// "@ondemand.eu-central-1.saucelabs.com/wd/hub";
	public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY
			+ "@ondemand.us-west-1.saucelabs.com/wd/hub";

	public int Executed = 0, Passed = 0, Failed = 0, Error = 0, Skipped = 0;
	public String ErrMsg = "", Note = "";
	
//	 public static String environment = System.getProperty("VERIFY_ENV"); 
//	 public static String Brands = System.getProperty("BRAND"); 
//	 public static String Priority_Val = System.getProperty("PRIORITY_VAL"); 
//	 public static String Test_Type = System.getProperty("TESTTYPE"); 
//	 public static String Cookie_Val =System.getProperty("COOKIE_VAL"); 
//	 public static String MFE_Cookie_Val =System.getProperty("MFE_COOKIE_VAL");
	
	
	
	
	public static String Brands = ""; 
	public static String environment ="uat1"; 
	public static String Priority_Val = "1"; 
	public static String Test_Type ="" ; 
	public static String Cookie_Val=""; 
	public static String MFE_Cookie_Val="";
	
	
	
	
	public static String build = "";
	public static String browser = "CH";
	public static String tax = "";
	public static int priorityValue;
	String DataFilePath = "./resource/CSP18F_Functionality";
	String ResultPath = "./target/Results/CSP18F_Functionality/";
	String ResultSheetName = "Functionality_Results";
	String ResultSheet = ResultPath + ResultSheetName + ".xls";
	String stacktrace = "";
	long startTime;
	GVar gvar = new GVar();
	boolean Skipflag = false;
	public String[] FPTFlexDate = new String[] { "13/02/2023" };
	
	
	
	
	
	@DataProvider(name = "Brand", parallel = true)
	public Object[][] dataProviderMethod() {

		if (Brands.equals("BRS") || Test_Type.equals("BRS")) {
			return new Object[][] { { "HD" }, { "WLF" }, { "STY" } };
		} else if (Brands.equals("GiftList") || Test_Type.equals("GiftList")) {
			return new Object[][] { { "HD" }, { "WLF" }, { "STY" }, { "TPF" }, { "CCO" } };
		} else if (!(Brands.equals("") || Brands.equals(null))) {
			return new Object[][] { { Brands } };
		} else {
			return new Object[][] {  { "18B" },    { "SCH" }, 
					 { "FBQ" }, { "BRY" }, };
		}
	}
	
	
	@AfterSuite(alwaysRun = true)
	public void DisplayResults() {
		if (Brands.equals(""))
			Brands = " 18B, CCO, TPF, HD, STY, WLF ,SCH, VIT, FBQ & BRY  ";
		int Skipped = Executed - Passed - Failed - Error;
		String ExecPriority = "";
		System.out.println("Executed TC: " + Executed + "\nPassed TC: " + Passed + "\nFailed TC: " + Failed
				+ "\nError TC: " + Error + "\nSkipped TC: " + Skipped);
		System.out.println("Skipped Tests :" + Skipped);
		System.out.println("Failures:\n" + ErrMsg);
		System.out.println("Note:\n" + Note);
		String Cookie_Value = Cookie_Val + "-" + MFE_Cookie_Val;

		if (priorityValue == 0)
			ExecPriority = "All";
		else
			ExecPriority = "Priority" + priorityValue;
		
		
		  HelperMethods.SlackMessage("csp" + Test_Type + "-" + Cookie_Value +
		  " Tests-" + browser + "-" + Brands + "-" + environment + "-" + ExecPriority,
		  Passed, Failed + Error, Skipped, ErrMsg, build, tax, Note);
		 
		
		 

	}
	
	
	
	@Test(enabled = true, dataProvider = "Brand", groups = { "All Brands" })
	public void TC001_cspVerifyHomePageSignIn(String Brand) throws Exception {
		String environment = "uat1";
		if ((priorityValue == 1) || (priorityValue == 0)) {
			Executed++;
				String Status = "Failed";String Result = "failed";
			startTime = System.currentTimeMillis();
			int TC_No = 1;
			GVar gvar = new GVar();
			gvar.setDataFilePath(DataFilePath);
			gvar.setResultPath(ResultPath);
			HelperMethods com_func = new HelperMethods(gvar);
			ArrayList<String> rowData = com_func.readXlsRow(TC_No);
			rowData.set(11, Brand);
			gvar.setRowData(rowData);
			RemoteWebDriver driver = null;
			SetDesiredCapabilities desCap = new SetDesiredCapabilities(gvar);
			gvar.setBrand(Brand);
			gvar.setPlatform(browser);
			gvar.setTCName(gvar.getTCName() + "_" + gvar.getBrand());
			driver = desCap.MutCap(gvar.getPlatform(), TC_No, gvar.getTCName(), Test_Type, environment, driver, URL);
			
			
			build = gvar.getBuild();
			browser = gvar.getPlatform();
			System.out.println(gvar.getSrNumber() + "_" + gvar.getTCName() + "::Started Execution");
			WelcomeSignInPage welcomeSIpage = new WelcomeSignInPage(driver, gvar);
			SignIn_Registerpage  SignInRegisterpage = new SignIn_Registerpage(driver, gvar);
			StoreFront storefront = new StoreFront(driver, gvar);
		
			try {
				com_func.EnvironmentURL(environment, gvar.getBrand());
				driver.get(gvar.getUrl());
				com_func.VerifyCookieFuntionality(environment, driver, gvar.getUrl(), Cookie_Val);
				com_func.SetMFEcookie(Brands, environment, driver, gvar.getUrl(), MFE_Cookie_Val);
				com_func.SetMFEcookie(Brands,environment, driver,
				gvar.getUrl(),MFE_Cookie_Val);
				gvar.setLineItem(0);
				welcomeSIpage.launchCSPURLNewTab();
				SignInRegisterpage.Auth0SignIn();
				storefront.verifyTitle();
				storefront.verifyService();
//				storefront.verifySales();
				storefront.verifyExperimental();
				Thread.sleep(2000);
				storefront.verifyLogoutFunction();
				
				if (!gvar.getComment().contains("Failed")) {
					Status = "Passed";Passed++;Result="passed";
					HelperMethods.makeFullScreenshot(driver, (gvar.getResultPath() + "/Passed Screenshots/"
							+ gvar.getSrNumber() + "_" + gvar.getTCName() + "_" + ".jpg"));
				} else {
					Failed++;
					String executionUrl = "URL - https://app.saucelabs.com/tests/"
							+ String.format((((RemoteWebDriver) driver).getSessionId()).toString()) + "\n";
					ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() + ":" + gvar.getComment() + "\n"
							+ executionUrl;
					HelperMethods.makeFullScreenshot(driver, (gvar.getResultPath() + "/Failed Screenshots/"
							+ gvar.getSrNumber() + "_" + gvar.getTCName() + "_" + ".jpg"));
				}
			}

			catch (Exception e) {
				Status = "Error";
				Error++;
				gvar.setComment(gvar.getComment() + e.getMessage());
				String executionUrl = "URL - https://app.saucelabs.com/tests/"
						+ String.format((((RemoteWebDriver) driver).getSessionId()).toString()) + "\n";
				HelperMethods.makeFullScreenshot(driver, (gvar.getResultPath() + "/Error Screenshots/"
						+ gvar.getSrNumber() + "_" + gvar.getTCName() + "_" + ".jpg"));
				ErrMsg += com_func.ErrorMessage(e, driver) + executionUrl;

			} finally {driver.executeScript("sauce:job-result=" + Result);

				System.out.println("Pass/Fail status: " + gvar.getTCName() + " - " + Status);
				BigDecimal bd = new BigDecimal((System.currentTimeMillis() - startTime) / 1000);
				String WriteData[] = { gvar.getSrNumber(), gvar.getPlatform(), Status, gvar.getTCName(),
						gvar.getSignInEmail(), gvar.getSignInPswd(), driver.getCurrentUrl(), Brand,
						bd.setScale(1, BigDecimal.ROUND_HALF_UP).toString(), gvar.getComment() };
				com_func.WriteToExcelFunctionality("/" + ResultSheetName + ".xls", WriteData);
				if (driver != null) {
					com_func.deleteCookies(driver);
					driver.quit();
				}
			}
		}
	}

	@Test(enabled = true, dataProvider = "Brand", groups = { "All Brands" })
	public void TC002_chatConnectToCSP(String Brand) throws Exception {
		String environment = "uat1";
		if ((priorityValue == 1) || (priorityValue == 0)) {
			Executed++;
				String Status = "Failed";String Result = "failed";
			startTime = System.currentTimeMillis();
			int TC_No = 2;
			GVar gvar = new GVar();
			gvar.setDataFilePath(DataFilePath);
			gvar.setResultPath(ResultPath);
			HelperMethods com_func = new HelperMethods(gvar);
			ArrayList<String> rowData = com_func.readXlsRow(TC_No);
			rowData.set(11, Brand);
			gvar.setRowData(rowData);
			RemoteWebDriver driver = null;
			SetDesiredCapabilities desCap = new SetDesiredCapabilities(gvar);
			gvar.setBrand(Brand);
			gvar.setPlatform(browser);
			gvar.setTCName(gvar.getTCName() + "_" + gvar.getBrand());
			driver = desCap.MutCap(gvar.getPlatform(), TC_No, gvar.getTCName(), Test_Type, environment, driver, URL);
			
			
			build = gvar.getBuild();
			browser = gvar.getPlatform();
			System.out.println(gvar.getSrNumber() + "_" + gvar.getTCName() + "::Started Execution");
			WelcomeSignInPage welcomeSIpage = new WelcomeSignInPage(driver, gvar);
			SignIn_Registerpage  SignInRegisterpage = new SignIn_Registerpage(driver, gvar);
			StoreFront storefront = new StoreFront(driver, gvar);
			HomePageCSP csphomepage = new HomePageCSP(driver, gvar);
			HomePageBrands brandhomepage = new HomePageBrands(driver, gvar);
			PopUp popup = new PopUp(driver, gvar);
			try {
				com_func.EnvironmentURL(environment, gvar.getBrand());
				driver.get(gvar.getUrl());
				com_func.VerifyCookieFuntionality(environment, driver, gvar.getUrl(), Cookie_Val);
				com_func.SetMFEcookie(Brands, environment, driver, gvar.getUrl(), MFE_Cookie_Val);
//				 com_func.SetMFEcookie(Brands,environment, driver,
//				 gvar.getUrl(),MFE_Cookie_Val);
				gvar.setLineItem(0);
				welcomeSIpage.launchCSPURLNewTab();
				SignInRegisterpage.Auth0SignIn();
				storefront.selectService();
				csphomepage.changeStatus();
				brandhomepage.openChatBrandHomePage();
				if (!gvar.getComment().contains("Failed")) {
				csphomepage.agentToCustomerMessage();
				if (!gvar.getComment().contains("Chat ACCEPT Popup is not displaying at CSP Homepage after Live agent connected in brand Homepage Chat - Failed")) {
				brandhomepage.verifyChatBrandHomePage();
				brandhomepage.customerToAgentMessage();
				csphomepage.verifyChatCSPHomePage();
				brandhomepage.verifyChatDisconnectFromBrandHomePage();
				}
				else 
					gvar.getComment().contains("Chat ACCEPT Popup is not displaying at CSP Homepage after Live agent connected in brand Homepage Chat - Failed");

				}
				else 
					gvar.getComment().contains("Live agent is not connected in brand Homepage Chat - Failed");
				
				if (!gvar.getComment().contains("Failed")) {
					Status = "Passed";Passed++;Result="passed";
					HelperMethods.makeFullScreenshot(driver, (gvar.getResultPath() + "/Passed Screenshots/"
							+ gvar.getSrNumber() + "_" + gvar.getTCName() + "_" + ".jpg"));
				} else {
					Failed++;
					String executionUrl = "URL - https://app.saucelabs.com/tests/"
							+ String.format((((RemoteWebDriver) driver).getSessionId()).toString()) + "\n";
					ErrMsg += gvar.getSrNumber() + "_" + gvar.getTCName() + ":" + gvar.getComment() + "\n"
							+ executionUrl;
					HelperMethods.makeFullScreenshot(driver, (gvar.getResultPath() + "/Failed Screenshots/"
							+ gvar.getSrNumber() + "_" + gvar.getTCName() + "_" + ".jpg"));
				}
			}

			catch (Exception e) {
				Status = "Error";
				Error++;
				gvar.setComment(gvar.getComment() + e.getMessage());
				String executionUrl = "URL - https://app.saucelabs.com/tests/"
						+ String.format((((RemoteWebDriver) driver).getSessionId()).toString()) + "\n";
				HelperMethods.makeFullScreenshot(driver, (gvar.getResultPath() + "/Error Screenshots/"
						+ gvar.getSrNumber() + "_" + gvar.getTCName() + "_" + ".jpg"));
				ErrMsg += com_func.ErrorMessage(e, driver) + executionUrl;

			} finally {driver.executeScript("sauce:job-result=" + Result);

				System.out.println("Pass/Fail status: " + gvar.getTCName() + " - " + Status);
				BigDecimal bd = new BigDecimal((System.currentTimeMillis() - startTime) / 1000);
				String WriteData[] = { gvar.getSrNumber(), gvar.getPlatform(), Status, gvar.getTCName(),
						gvar.getSignInEmail(), gvar.getSignInPswd(), driver.getCurrentUrl(), Brand,
						bd.setScale(1, BigDecimal.ROUND_HALF_UP).toString(), gvar.getComment() };
				com_func.WriteToExcelFunctionality("/" + ResultSheetName + ".xls", WriteData);
				if (driver != null) {
					com_func.deleteCookies(driver);
					driver.quit();
				}
			}
		}
	}
	
}

